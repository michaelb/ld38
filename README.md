
     ____  _     ___   ____ _  ___     ___ _   _  ____ ____  
    | __ )| |   / _ \ / ___| |/ / |   |_ _| \ | |/ ___/ ___| 
    |  _ \| |  | | | | |   | ' /| |    | ||  \| | |  _\___ \ 
    | |_) | |__| |_| | |___| . \| |___ | || |\  | |_| |___) |
    |____/|_____\___/ \____|_|\_\_____|___|_| \_|\____|____/ 


[A puzzle game made for LD Jam 38](https://ldjam.com/events/ludum-dare/38/$15470)

You can use the following FOSS package to create / edit levels:

* michaelb/godot-voxmod-edit


