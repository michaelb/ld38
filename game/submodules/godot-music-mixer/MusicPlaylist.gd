export(bool) var autoplay = false
export(bool) var prefer_pause = false
export(bool) var repeat_one = false
export(float, 0.0, 60.0) var crossfade = 0.0

# TODO: SHUFFLE LOGIC is broken, incompatible with crossfade. Need to either
# actually order playlist, or do something more complicated
export(int, 'No shuffle', 'Shuffle', 'Shuffle Once')  var shuffle = 0


export(int, 'Crossfade', 'Fade out only', 'Fade in only')  var crossfade_style = 0
const CROSSFADE = 0
const FADE_OUT_ONLY = 1
const FADE_IN_ONLY = 2

const Utils = preload('./Utils.gd')

const NO_SHUFFLE = 0
const SHUFFLE_RANDOM = 1
const SHUFFLE_ONCE = 2
onready var timer = get_node('LoadNextTimer')

const TINY_SIZE = 0.01 # Used for rounding when pausing v stopping
const REALTIME_MARGIN = 1

var has_shuffled = false

func _ready():
    _stop_all()
    if autoplay:
        play()

func play_child(name):
    stop()
    var track = get_node(name)
    move_child(track, 0) # move to top
    play()

# Enqueues the given node path as the next node that will play.  Should
# only call this while not crossfading.
func play_next(name):
    var track = get_node(name)
    move_child(track, 1) # move to "next slot"

var force_fade_in_track = null
var force_fade_out_track = null
var force_crossfade = 0.0
func crossfade_to_child(name):
    if crossfade == 0.0:
        Utils.warn("Attempting to crossfade when disabled.")
        play_child(name)
        return

    # Move the current track to the back
    force_fade_out_track = get_current_track()
    move_child(force_fade_out_track, get_children().size() - 1)

    # Move the specified child to top
    force_fade_in_track = get_node(name)
    move_child(force_fade_in_track, 0) # move to top

    # Start forced crossfade
    force_crossfade = crossfade

    set_process(true)

func shuffle_once():
    # Fisher-yates shuffle
    Utils.shuffle_node_in_place(self)

#####################
# Private functions to manage the timer.
# The timer is used to switch on and off "set_process" to avoid overhead of
# GDScript. Essentially, only when its close to mattering do we do processing.
func get_realtime_margin():
    return REALTIME_MARGIN + crossfade

func _refresh_timer():
    set_process(false)
    if is_paused() or not is_playing():
        return timer.stop()

    if not timer:
        Utils.warn("Not added to scene.")
        return

    # Get time, always waiting at least 1 hundredth of a second
    var track = get_current_track()
    var timeleft = track.get_length() - track.get_pos() - get_realtime_margin()
    var time = max(timeleft, 0)
    if time == 0:
        set_process(true)
    else:
        # switch off realtime, only to turn on close to the end of the track
        timer.set_wait_time(time)
        timer.start()

func _on_timeout():
    set_process(true) # switch to realtime
#####################

func _process(delta):
    ##########################
    # Handle forced fade between of tracks
    if force_crossfade > 0.0:
        force_crossfade -= delta
        if force_crossfade < 0.0:
            # Stop the forced crossfade
            force_fade_out_track.stop()
            force_fade_out_track = null
            force_fade_in_track = null
            force_crossfade = 0.0
            play() # Ensure top is playing
            return

        # Forced crossfade of tracks
        # TODO apply easing here
        var crossfade_ratio = (crossfade - force_crossfade) / crossfade

        # ensures tracks are playing
        _resume_track(force_fade_in_track)
        _resume_track(force_fade_out_track)
        if crossfade_style != FADE_OUT_ONLY:
            force_fade_in_track.set_volume(crossfade_ratio)
        if crossfade_style != FADE_IN_ONLY:
            force_fade_out_track.set_volume(1 - crossfade_ratio)
        return

    ##########################
    # Handle normal case: fading between tracks in the queue, or 
    var track = get_current_track()
    var timeleft = track.get_length() - track.get_pos()
    if timeleft > get_realtime_margin():
        _refresh_timer()
        return

    if timeleft > crossfade:
        return # Still time left, not crossfading, nothing to do

    # TODO: add signal for custom playlist behavior
    if crossfade > 0:
        var next_track = get_next_track()
        # TODO apply easing here
        var crossfade_ratio = (crossfade - timeleft) / crossfade

        # Ensure we aren't just repeating the same one
        if next_track != track:
            _resume_track(next_track) # ensures next track is playing
            if crossfade_style != FADE_OUT_ONLY:
                next_track.set_volume(crossfade_ratio)
            if crossfade_style != FADE_IN_ONLY:
                track.set_volume(1 - crossfade_ratio)

    if timeleft == 0:
        # Trigger switching of current track to next one
        if repeat_one:
            restart_current_track()
        else:
            load_next()

func restart_current_track():
    stop() # Play same item again
    play()

func load_next():
    stop() # First stop current track

    var track = get_current_track()

    # Move current track to back
    move_child(track, get_children().size() - 1)

    play() # start playing the next one

    # And shuffle the remaining tracks, if applicable
    if shuffle == SHUFFLE_RANDOM:
        shuffle_once()

###
# Return total length in seconds of all songs in playlist
func get_length():
    var sum = 0.0
    for track in get_tracks():
        sum += track.get_length()
    return sum

func get_pos():
    return 0.0 # always at front, since it makes no sense

func get_buffering_msec():
    var track = get_current_track()
    return track.get_buffering_msec()

func get_tracks():
    var tracks = []
    for child_node in get_children():
        if child_node.get_name() == 'LoadNextTimer':
            # Skip over this invalid child
            continue
        tracks.append(child_node)
    return tracks

func get_current_track():
    var tracks = get_tracks()
    if tracks.size() < 1:
        Utils.warn("Empty playlist")
        return null
    return tracks[0]

func get_next_track():
    var tracks = get_tracks()
    if tracks.size() < 1:
        Utils.warn("Empty playlist")
        return null
    if tracks.size() == 1 or repeat_one:
        return tracks[0] # just return same track
    return tracks[1]

func _resume_track(track):
    if track.is_paused():
        track.set_paused(false)
    if not track.is_playing():
        var time_left = track.get_length() - track.get_pos()
        if time_left > 0:
            track.play()
        else:
            track.play(0) # ensure starts from beginning

###########################
# Wraps around top track with these functions
func play():
    var track = get_current_track()
    if not track:
        return

    if shuffle != NO_SHUFFLE:
        if not has_shuffled:
            has_shuffled = true
            shuffle_once()
            track = get_current_track()

    track.set_volume(1.0) # ensure back to max volume
    _resume_track(track)
    _refresh_timer()

func _is_over(track):
    return (track.get_length() - track.get_pos()) < TINY_SIZE

func stop():
    var track = get_current_track()
    if not track:
        return

    if prefer_pause and not _is_over(track):
        track.set_paused(true)
    else:
        track.set_paused(false)
        track.stop()
    track.set_volume(1.0) # ensure back to max volume
    _refresh_timer()

func set_paused(value):
    var track = get_current_track()
    if not track:
        return
    track.set_paused(value)
    _refresh_timer()

var volume = 1.0
func set_volume(value):
    volume = value
    var track = get_current_track()
    if not track:
        return
    track.set_volume(value)

func get_volume():
    return volume

func is_paused():
    var track = get_current_track()
    if not track:
        return false
    return track.is_paused()

func is_playing():
    var track = get_current_track()
    if not track:
        return false
    return track.is_playing()

func _stop_all():
    # Ensure all are stopped
    for track in get_tracks():
        track.stop()
