static func shuffle_node_in_place(node):
    # Fisher-Yates shuffle
    var length = node.get_child_count()
    if length < 2:
        return # No-op if 1 or less items

    var index = length - 1
    while index > 0:
        var selected = randi() % index
        var index_item = node.get_child(index)
        var selected_item = node.get_child(selected)
        node.move_child(index_item, selected)
        node.move_child(selected_item, index)
        index -= 1

static func warn(message):
    print("MusicPlaylist: WARNING: " + message)
