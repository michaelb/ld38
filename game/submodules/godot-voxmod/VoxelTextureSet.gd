const Utils = preload("./Utils.gd")
const voxel_material = preload("./materials/VoxelMaterial.tres")

var texture_list
var size

var count
var uv_coefficient

var image

var offsets_by_id = {}

func _init(size):
    self.size = int(size)

func load_textures(texture_dict):
    # Loads a bunch of textures, keyed by ID
    var texture_list = []

    var image_i = 0
    for texture_id in texture_dict:
        # Creates UV offsets based on the 9x assumption
        #uvs_by_id[texture_id] = Vector2(size * (image_i + 1), size)
        #uvs_by_id[texture_id] = Vector2(size * (image_i + 1), size)
        #offsets_by_id[texture_id] = Vector2(size * (image_i + 1), size)
        offsets_by_id[texture_id] = Vector2(image_i + 1, 1)
        image_i += 3
        texture_list.append(texture_dict[texture_id]) # Add to list in order

    # Finally, combine the sub-images into the final larger image
    self.image = Utils.combine_images(size, texture_list)

    # Total number of textures
    count = texture_list.size()

    # The coefficient to multiply the offset to get the coords for the top-left
    # corner of each texture
    uv_coefficient = Vector2(1.0 / (3 * count), 1.0 / 3)

func get_uv_for(mid):
    if not offsets_by_id.has(mid):
        print("ERROR: Could not find material for '" + mid + "'")
    return offsets_by_id[mid]

func make_image_texture():
    var tex = ImageTexture.new()
    tex.create_from_image(image, 0) # STORAGE_RAW
    return tex

func make_fixed_material():
    # Creates a FixedMaterial based on the combined images
    var new_fixed_material = voxel_material.duplicate()
    print('this is new fixed material, ', new_fixed_material)
    # for now, just PARAM_DIFFUSE
    new_fixed_material.set_texture(0, make_image_texture())
    return new_fixed_material
