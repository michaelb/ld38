# TODO:
# Needs to be rewritten to be either loaded from a file (cleanly), or
# instantiated from out of the blue.
# Does not need to be aware of tile info!

#const SchemaConf = preload('./SchemaConf.gd')
const Vox3D = preload('../godot-voxtools/Vox3D.gd')
const ShapedVox3D = preload('../godot-voxtools/ShapedVox3D.gd')
const Vox3DConsts = preload('../godot-voxtools/Consts.gd')

var path

var _blocks = null
var conf_data = {}
var tiles_by_id = {}
var initial_entities = {}
var entity_manager = null

# Material:
var material_vox3d = null
var shape_vox3d = null

# Misc settings:
var disable_bottom_and_back = true

var width
var height
var depth
var array_size
var base_path
var EMPTY = Vox3DConsts.EMPTY

var max_x
var max_y
var max_z

func load_from(conf_data):
    #self.tiles_by_id = tiles_by_id
    self.conf_data = conf_data
    self.base_path = conf_data['_path'].get_base_dir()
    self._do_load()

func setup(info):
    # New load procedure stuff:
    self.base_path = info['__path'].get_base_dir()
    set_dimensions(int(info['max_x']), int(info['max_y']), int(info['max_z']))
    var vector = get_world_dimension_vector()
    width = vector.x
    height = vector.y
    depth = vector.z

    # And read in the chunk blocks
    var chunk_data_path = base_path.plus_file(info['path'])
    var f
    f = File.new()
    var err = f.open(chunk_data_path, File.READ)
    if not f.is_open():
        print("ERROR: Can't open voxel data ", chunk_data_path)
    else:
        _blocks = RawArray()
        array_size = width * height * depth
        _blocks = f.get_buffer(array_size)
        material_vox3d.copy_from(_blocks) # copy from the other raw array
        # TODO: separate material from shape:
        shape_vox3d.copy_from(_blocks) # copy from the other raw array
        f.close()

func _do_load():
    #conf_data = conf.open(path)

    # Now set "height" "width" and "depth"
    var d = conf_data['data'][0]
    set_dimensions(int(d['max_x']), int(d['max_y']), int(d['max_z']))
    var vector = get_world_dimension_vector()
    width = vector.x
    height = vector.y
    depth = vector.z

    # Set defaults
    #if not conf_data.has('entity_template'):
    #    conf_data['entity_template'] = []
    #if not conf_data.has('entity'):
    #    conf_data['entity'] = []

    initial_entities = conf_data['entity'] # TODO: should probably move this somewhere else

    # Now set up an entity manager
    #entity_manager = EntityManager.new(conf_data['entity_template'], conf_data['entity'])

    # And read in the chunk blocks
    var chunk_data_fn = conf_data['data'][0]['path']
    var chunk_data_path = base_path.plus_file(chunk_data_fn)
    var f
    f = File.new()
    var err = f.open(chunk_data_path, File.READ)
    if not f.is_open():
        print("ERROR: Can't open voxel data ", chunk_data_path)
    else:
        _blocks = RawArray()
        array_size = width * height * depth
        _blocks = f.get_buffer(array_size)
        material_vox3d.copy_from(_blocks) # copy from the other raw array
        # TODO: separate material from shape:
        shape_vox3d.copy_from(_blocks) # copy from the other raw array
        f.close()

func save_world_data():
    print("NOT IMPLEMENTED YET")
    return
    # And read in the chunk blocks
    var chunk_data_path = self.path + '.chunk_data'
    var f
    f = File.new()
    var err = f.open(chunk_data_path, File.WRITE)
    if not f.is_open():
        print("OH NO, cannot open ", chunk_data_path)
        return
    f.store_buffer(_blocks)
    f.close()

func trace_ray_collision(origin_v3, direction_v3):
    # note: Later ensure it uses the terrain collision d3d
    var result = shape_vox3d.trace_ray_collision(origin_v3, direction_v3)
    return result

#
# Sets the given dimensions, clearing all content
func set_dimensions(max_x, max_y, max_z):
    self.max_x = max_x
    self.max_y = max_y
    self.max_z = max_z

    # Set up volume dict
    material_vox3d = Vox3D.new(max_x, max_y, max_z)
    shape_vox3d = ShapedVox3D.new(max_x, max_y, max_z)
    # Make out of bounds solid, and warn when used
    #vox3d.set_out_of_bounds(Vox3D.CUBE, true) # More correct
    material_vox3d.set_out_of_bounds(EMPTY, false)
    shape_vox3d.set_out_of_bounds(EMPTY, false)

func get_world_dimension_vector():
    #var d = conf_data['data'][0]
    return Vector3(max_x, max_y, max_z)

const BOUNDARY = 1
func is_out_of_bounds(vec):
    # TODO use built-in math
    # NOTE: uses 1 block boundary. This is to prevent Physics.gd from throwing
    # an error when the moment propels a bullet past the boundary. In reality,
    # this should be checked for in physics.
    var d = get_world_dimension_vector() - Vector3(BOUNDARY, BOUNDARY, BOUNDARY)
    return vec.x < BOUNDARY or vec.y < BOUNDARY or vec.z < BOUNDARY or vec.x >= d.x or vec.y >= d.y or vec.z >= d.z

func get_index(vector):
    # TODO: probably isn't used any more, should b removed
    #return int((vector.x * depth * height) + (vector.y * depth) + vector.z)
    return int(vector.x + (vector.z * width) + (vector.y * depth * width))

func get_block(vector):
    return material_vox3d.get_by_vector(vector)

func get_block_material(vector):
    return material_vox3d.get_by_vector(vector)

#
# After loaded, add an additional block
func add_block(vector, type):
    material_vox3d.init_block(vector.x, vector.y, vector.z, type)
    shape_vox3d.init_block(vector.x, vector.y, vector.z, 1)

func remove_block(vector):
    material_vox3d.init_block(vector.x, vector.y, vector.z, EMPTY)

#
# Returns true if its exposed on any side
func is_block_exposed(v):
    var sides = [
        Vector3(1,  0,  0),
        Vector3(0,  1,  0),
        Vector3(0,  0,  1),
        Vector3(-1, 0,  0),
        Vector3(0, -1,  0),
        Vector3(0,  0, -1),
    ]
    for side in sides:
        var val = material_vox3d.get_by_vector(v + side)
        if val == EMPTY:
            return true
    return false

#
# Returns a list of 1s and 0s indicating which cube faces should be
# drawn, based on what is exposed
func get_needed_cube_faces(v):
    #    needed_faces is structured x (left,right) y (up,down) z (front, back) [0,0,1,0,0,0]
    var sides = [
        Vector3(1,  0,  0),
        Vector3(-1, 0,  0),
        Vector3(0,  1,  0),
        Vector3(0, -1,  0),
        Vector3(0,  0,  1),
        Vector3(0,  0, -1),
    ]
    var needed_cube_faces = [0, 0, 0, 0, 0, 0]
    var normals = [Vector3(1, 0, 0), Vector3(0, 1, 0), Vector3(0, 0, 1)]
    var negative_positive = [1, -1]
    for i in range(6):
        var val = material_vox3d.get_by_vector(v + sides[i])
        if val == EMPTY:
            needed_cube_faces[i] = 1

    # NOTE: never renders bottom or back face, also can be enhanced by
    # not rendering at the edge of TChunk (e.g. an outer wall)
    if disable_bottom_and_back:
        needed_cube_faces[3] = 0
        needed_cube_faces[5] = 0
    return needed_cube_faces
