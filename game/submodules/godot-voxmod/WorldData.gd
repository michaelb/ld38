# TODO: Basically, allow multiple world generators
# 1. It looks through them in a list for each block. They each have a range of locations maybe...?
# 2. Currently, two types:
#    a. Plain  (just a y = 1 plain)
#    b. VolData (handles the first voldata on modloader...)
# 3. (b) is higher prio, so it goes first, if falls through it goes to (a)

# TOOD: Flesh this out. New strategy: Gen VolDatas based on algo, or loading.
# Then, change Terrain to support only creating necessary amounts of TChunks,
# and unloading TChunks if they move out of view

# Either way, it should be seamless with voldata except it gens everything

const VolData = preload('./VolData.gd')


signal load_chunk
signal unload_chunk


var world_gens = []
var _tmp_voldata

# Sets
func setup(modloader):
    world_gens = modloader.get_all('world')
    #print("THIS IS WORLD GENS", world_gens)
    _tmp_voldata = modloader.vol_datas.values()[0]

#func handle_load_chunk(x, y, z):
#    pass

#func handle_unload_chunk(x, y, z):
#    pass

func set_center(x, y, z):
    pass

func set_load_radius(r):
    pass

const EMPTY = 0 # TODO remove

# Map chunks
const CHUNK_D = Vector3(64, 64, 64)
var voldata_chunks = {}

func get_chunk(vector):
    var chunk_vector = (vector / CHUNK_D).floor()
    if not voldata_chunks.has(chunk_vector):
        voldata_chunks[chunk_vector] = gen_chunk(chunk_vector)
    return voldata_chunks[chunk_vector]

#
# Given a global vector, returns the localized version for a chunk
func localize(global_vector):
    var chunk_vector = (global_vector / CHUNK_D).floor()
    var offset = chunk_vector * CHUNK_D
    return global_vector - offset

func gen_chunk(chunk_vector):
    # 1. Eventually allow with serializing / deserializing from disk (saving  +
    # loading state) from within WorldData
    # 2. World.gd (generators)  that are prefab chunk based will simply
    # deserialize chunks from templates
    # For now, only 1 world gen allowed per WorldData chunk
    var voldata = VolData.new()
    voldata.set_dimensions(CHUNK_D.x, CHUNK_D.y, CHUNK_D.z)
    var found = false
    for world in world_gens:
        if world.check_chunk(chunk_vector):
            found = true
            world.gen_chunk(voldata, chunk_vector)
            break

    if not found:
        print("ERROR: WorldData.gd: No world gen found for " + str(chunk_vector))
    return voldata


# Also, implement the interface from VolData, as such:
func get_block(global_vector):
    return get_chunk(global_vector).get_block(localize(global_vector))

# Also, implement the interface from VolData, as such:
func get_block_material(global_vector):
    return get_chunk(global_vector).get_block_material(localize(global_vector))

func is_block_exposed(global_vector):
    return get_chunk(global_vector).is_block_exposed(localize(global_vector))

func get_needed_cube_faces(global_vector):
    return get_chunk(global_vector).get_needed_cube_faces(localize(global_vector))

func set_block(global_vector, material):
    return get_chunk(global_vector).set_block(localize(global_vector), material)

func delete_block(global_vector):
    return get_chunk(global_vector).delete_block(localize(global_vector))

# TODO: Remove this eventually, when it gets removed from surrounding, for
# infinite world
func get_world_dimensions():
    return _tmp_voldata.get_world_dimensions()

func trace_ray_collision(origin, ray):
    return null # always return null

# `world_data.get_block(vector)`
# `world_data.EMPTY`
# `world_data.is_block_exposed(vector)`
# `world_data.get_needed_cube_faces(vector)`
# `world_data.trace_ray_collision(origin, ray)`
# `world_data.set_block(vector, material)`
# `world_data.delete_block(vector)`
# `world_data.get_world_dimension_vector()` - total size of world


#    chunk, that should be connected to the world gen / loading algo
#* `signal unload_chunk(x, y, z)` -- inverse of above, should do a "diff" to
#    see if dirty, and if dirty, serialize to disk
#* `set_center(x, y, z)` -- Move simulation center. This in turn triggers
#    `load_chunk` and `unload_chunk`
#* `set_chunk_radius(r)` -- Sets a chunk radius, e.g. autloads within `r`
#    chunks around center







