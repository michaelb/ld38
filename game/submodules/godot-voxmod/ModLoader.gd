const SchemaConf = preload("./SchemaConf.gd")
const VolData = preload("./VolData.gd")
const WorldData = preload('./WorldData.gd')
const Utils = preload('Utils.gd')
const Vox3DUtils = preload('../godot-voxtools/Utils.gd')
const VoxelTextureSet = preload("./VoxelTextureSet.gd")
const World = preload("./types/World.gd")
const Pattern = preload("./types/Pattern.gd")

const BUILT_IN_SCHEMAS = {
    'world': {
        '_hook': 'World',
        '_optional': 'pattern',
        'pattern': 'pattern',
    },
    'voldata': {
        '_hook': 'VolData',
    },
    'mod': {},
    'pattern': {
        '_hook': 'Pattern',
        '_optional': 'material,terrain',
        'material': 'vox3d',
        'terrain': 'shapedvox3d',
    },
}

var texset
var file_list = []

var _material_meta = null
var _current_path = null

var datatypes = {}
var objs = {}
var objs_by_filepath = {}

#
# Instantiates a new object based on the class name. Classes aren't
# first class citizens in Godot script, so this will have to do.
func _instance_from_class_name(name):
    if name == 'World':
        return World.new()
    if name == 'VolData':
        return VolData.new()
    if name == 'Pattern':
        return Pattern.new()
    print("ModLoader.gd: WARNING: Unknown hook " + name)
    return null

func _init():
    # Init datatypes with the built-in ones
    for name in BUILT_IN_SCHEMAS:
        datatypes['name'] = name
        datatypes[name] = BUILT_IN_SCHEMAS[name]
        objs[name] = {}


#
# Get the object of datatype `type' with name attribute `name'
func get(type, name):
    return objs[type][name]

#
# Return a list of all objects registered of datatype `type'. An empty list is
# returned if there exist none.
func get_all(type):
    if not objs.has(type):
        return []
    return objs[type].values()

#
# Loads the given path, recursing through sub-directories loading all .cfg files
func load_dir(path):
    var _postponed = []

    var files = Utils.recursive_files_list(path)
    for file_path in files:
        if not file_path.ends_with('.cfg'):
            # print('skipping non CFG file: ', file_path)
            continue
        file_list.append(file_path)

        # print('loading ' + file_path)
        var sc = SchemaConf.new()
        var data = sc.open(file_path)
        data['_path'] = file_path
        _current_path = file_path

        # Always put maps off to last minute, since we want to do other stuff
        # first
        # TODO: remove this requirement
        if data.has('data'):
            # VoxMap data
            _postponed.append(data)
        else:
            load_data(data)

    # Do all postponed datas
    for postponed in _postponed:
        load_data(postponed)

    # Now, create a voxel texture set for all materials
    if _material_meta == null:
        Utils.warn("No material meta, cannot update materials from mod.")
    else:
        texset = VoxelTextureSet.new(_material_meta['size'])
        texset.load_textures(_material_paths_by_id)

const MATERIAL = 'material'
const ENTITY_TEMPLATE = 'entity_template'
const MATERIAL_META = 'material_meta'
const DATA = 'data'

func load_data(data):
    if data.has('datatype'):
        # Datatype definition, to be used later on
        for schema in data['datatype']:
            register_datatype(schema['name'], schema)
    if data.has('material'):
        for material in data['material']:
            load_material(material)
    if data.has('entity_template'):
        for entity_template in data['entity_template']:
            load_entity_template(entity_template)
    #if data.has('item'):
    #    for item in data['item']:
    #        load_item(item)
    if data.has(MATERIAL_META):
        load_material_meta(data[MATERIAL_META][0])
    if data.has('data'):
        # Handle map data stuff
        load_map_data(data)

    # Finally check custom datatypes
    for schema_name in datatypes:
        if data.has(schema_name):
            for instance in data[schema_name]:
                var schema = datatypes[schema_name]
                var path = data['_path']
                load_custom_datatype(schema_name, schema, instance, path)

func register_datatype(schema_name, schema):
    # For now that's all we do
    datatypes[schema_name] = schema
    objs[schema_name] = {}

var materials_by_id = {}
var _material_paths_by_id = {}
func load_material(info):
    if not info.has('id') or (not info.has('path') and not info.has('empty')):
        return Utils.warn('Invalid material: "' + str(info) + '"')
    # And set up a dict keying tiles by ID
    materials_by_id[int(info['id'])] = info
    if info.has('path'):
        var full_path = _current_path.get_base_dir().plus_file(info.path)
        _material_paths_by_id[int(info['id'])] = full_path
    if info.has('decal_tuft'):
        # Set up decal image
        var decal_path = _current_path.get_base_dir().plus_file(info.decal_tuft)
        #var image = load(decal_path)
        var image = Image(16, 16, false, Image.FORMAT_RGBA) # TODO FIX
        image.load(decal_path)
        var tex = ImageTexture.new()
        tex.create_from_image(image, 0) # STORAGE_RAW
        info['decal_tuft_texture'] = tex

func get_material_info():
    return materials_by_id

func load_material_meta(meta_info):
    _material_meta = meta_info

var entity_templates = {}
func load_entity_template(info):
    entity_templates[info.name] = info

var vol_datas = {}
func load_map_data(info):
    var voldata = VolData.new()
    voldata.load_from(info)
    var filename = info['_path'].get_file()
    vol_datas[filename] = voldata

#
# Loads an instance of a custom data type, given the schema name, the schema
# itself, and the data of the instance
func load_custom_datatype(schema_name, schema, info, path):
    var base_path = path.get_base_dir()

    for field in schema:
        if field.begins_with('_'): # special one, e.g. _hook, _optional
            continue
        if not info.has(field):
            if not schema.has('_optional') or schema['_optional'].find(field) == -1:
                print("ModLoader.gd: WARNING: Instanced does not have " + field)
            continue
        var loaded_data = null
        var fullpath = null

        # Load image for this pattern
        if schema[field] == 'image':
            fullpath = base_path.plus_file(info[field])
            loaded_data = load(fullpath)

        # Already-loaded pattern
        elif schema[field] == 'pattern':
            var pattern_name = info[field]
            loaded_data = get('pattern', pattern_name)
            fullpath = ''

        elif schema[field] == 'vox3d':
            fullpath = base_path.plus_file(info[field])
            loaded_data = vox3d_from_file(false, info, fullpath)

        elif schema[field] == 'shapedvox3d':
            fullpath = base_path.plus_file(info[field])
            loaded_data = vox3d_from_file(true, info, fullpath)

        if loaded_data != null:
            info[field] = loaded_data
            info[field + '__path'] = fullpath

    var result = info
    info['__type'] = schema_name
    info['__path'] = path
    if schema.has('_hook'):
        # Means we instantiate something after
        var obj = _instance_from_class_name(schema['_hook'])
        info['__obj'] = obj
        obj.setup(info)
        result = obj

    # Finally actually put the object into the final objs data structure
    objs[schema_name][info['name']] = result

    if not objs_by_filepath.has(path):
        objs_by_filepath[path] = []
    objs_by_filepath[path].append(info)

#
# Given info dict and path, use Vox3DUtils to load it from file
func vox3d_from_file(is_shaped, info, path):
    var max_x = int(info['x'])
    var max_y = int(info['y'])
    var max_z = int(info['z'])
    if info.has('data_x'):
        max_x = int(info['data_x'])
        max_y = int(info['data_y'])
        max_z = int(info['data_z'])
    return Vox3DUtils.from_file(is_shaped, path, max_x, max_y, max_z)

#
# Generates a new WorldData object
func generate_world():
    var wd = WorldData.new()
    wd.setup(self)
    return wd

#
# Save edited data from pattern to where it was loaded
func save_edited_static_pattern(pattern):
    var material_path = pattern.info['material__path']
    var terrain_path = pattern.info['terrain__path']
    Vox3DUtils.save_to_file(material_path, pattern.material)
    Vox3DUtils.save_to_file(terrain_path, pattern.terrain.vox3d)
