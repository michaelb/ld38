extends "./unittest.gd"

const ModLoader = preload("../ModLoader.gd")

func tests():
    testcase('Reads in basic mod')
    var modloader = ModLoader.new()
    var basedir = self.get_script().get_path().get_base_dir()
    modloader.load_dir(basedir.plus_file('data/testmod1/'))

    # check an 'customitem' type was registered along with a single item
    assert_eq(modloader.get_all('customitem').size(), 1, '1 custom item')
    var item = modloader.get('customitem', 'gem') # gets the gem item
    assert_ne(item, null, 'item gotten')
    assert_true(item['image'] extends Texture, 'image loaded')

    # check materials and vol_datas
    assert_eq(modloader.materials_by_id.size(), 4, '4 material types')
    assert_eq(modloader.vol_datas.size(), 1, '1 vol data')
    assert_eq(modloader.vol_datas.values()[0].max_x, 64, 'vol datas size is right')

    # check texset was created with expected size
    assert_ne(modloader.texset, null, 'texture set is created')
    var exp_size = Rect2(0, 0, 16*3*3, 16*3)
    assert_eq(modloader.texset.image.get_used_rect(), exp_size, 'texset has 3 tiles')
    endcase()

    testcase('Can create a WorldData')
    var world_data = modloader.generate_world()
    assert_eq(world_data.localize(Vector3(65, 65, 65)), Vector3(1, 1, 1), 'localize method works')
    assert_eq(world_data.world_gens.size(), 3, 'has three world generators')
    assert_eq(world_data.world_gens[0].type, 'tiled-pattern', 'world data 0 looks right')
    assert_eq(world_data.world_gens[1].type, 'voldata', 'world data 1 looks right')
    assert_eq(world_data.world_gens[2].type, 'test-pillar-plane', 'world data 2 looks right')
    assert_eq(world_data.world_gens[2].check_chunk(Vector3(0, 0, 0)), true, 'world data 2 looks right (2)')
    endcase()


    # Now test loading the pattern system
    testcase('World tiled-pattern is made')
    var w = world_data.world_gens[0]
    assert_ne(w.pattern, null, 'has a pattern assigned')
    assert_eq(w.pattern.name, 'testingpat', 'with expected name')
    endcase()

