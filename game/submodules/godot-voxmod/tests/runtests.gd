extends SceneTree

func _init():
    load('res://tests/unittest.gd').run([
        'tests.SchemaConf',
        'tests.ModLoader',
        'tests.Utils',
    ])
    quit()

