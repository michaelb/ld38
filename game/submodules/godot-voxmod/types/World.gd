# World algorithm
var type
var info
var pattern = null

# TODO should put into consts
const CHUNK_D = Vector3(64, 64, 64)

func setup(info):
    type = info['type']
    self.info = info
    #path = info['path'] # for 
    if type == 'tiled-pattern':
        pattern = info['pattern']

#
# Returns "true" if this generator should handle generating this chunk.
# Note: For now, only 1 world gen allowed per WorldData chunk, so if it returns
# true, it should generate the entire chunk.
func check_chunk(chunk_vector):
    if type == 'test-pillar-plane':
        return true # It can always gen if its a test pillar plane
    if type == 'tiled-pattern':
        return true # It can always gen if its an infini-tiled pattern
    return false

func _test_pillar_plane(voldata):
    print("World.gd: Generating test pillar plane")
    var max_x = voldata.max_x
    var max_z = voldata.max_z
    for x in range(max_x):
        for z in range(max_z):
            var vector = Vector3(x, 0, z)
            var type = 2
            voldata.add_block(vector, type)

    # And add in some random brick blocks
    randomize()
    for x in range(max_x):
        for z in range(max_z):
            var type = 1
            var r = randi()
            if r % 50 == 0:
                voldata.add_block(Vector3(x, 1, z), type)
            if r % 100 == 0:
                voldata.add_block(Vector3(x, 2, z), type)
    return voldata

func _tiled_pattern(voldata, chunk_vector):
    # TODO: have it offset based on chunk_vector
    #pattern.gen_chunk(voldata, chunk_vector)
    voldata.material_vox3d = pattern.material
    voldata.terrain_vox3d = pattern.terrain

func gen_chunk(voldata, chunk_vector):
    # For now, just gen plane
    if type == 'test-pillar-plane':
        _test_pillar_plane(voldata)
    elif type == 'tiled-pattern':
        _tiled_pattern(voldata, chunk_vector)

