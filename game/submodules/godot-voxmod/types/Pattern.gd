const Utils = preload('../Utils.gd');

# Pattern block. Either static or composite
var type
var info
var name

var max_x
var max_y
var max_z

var material
var terrain
var pattern

const EMPTY = 0 # TODO remove

func setup(info):
    type = info['type']
    name = info['name']
    self.info = info
    self.max_x = int(info['x'])
    self.max_y = int(info['y'])
    self.max_z = int(info['z'])

    if info.has('patterns'):
        pattern = info['patterns']
    else:
        material = info['material']
        terrain = info['terrain']

func gen_chunk(voldata, chunk_vector):
    # For now just do the pillar thing
    for x in range(max_x):
        for y in range(max_y):
            for z in range(max_z):
                var vector = Vector3(x, y, z)
                if not terrain.vector_in_bounds(vector):
                    voldata.add_block(vector, 0)
                    continue
                var mat = material.get(x, y, z)
                var terr = terrain.get(x, y, z)
                if terr != EMPTY:
                    voldata.add_block(vector, mat)

#
# VolData interface
func trace_ray_collision(origin_v3, direction_v3):
    # note: Later ensure it uses the terrain collision d3d
    var result = terrain.trace_ray_collision(origin_v3, direction_v3)
    return result

func get_world_dimension_vector():
    return Vector3(max_x, max_y, max_z)

func get_block_material(vector):
    return material.get_by_vector(vector)

func get_block(vector):
    return terrain.get_by_vector(vector)

# After loaded, add an additional block
func add_block(vector, type):
    terrain.init_block(vector.x, vector.y, vector.z, 1)
    material.init_block(vector.x, vector.y, vector.z, type)

func remove_block(vector):
    material.init_block(vector.x, vector.y, vector.z, EMPTY)
    terrain.init_block(vector.x, vector.y, vector.z, EMPTY)

func get_needed_cube_faces(v):
    return Utils.get_needed_cube_faces(terrain, v)

func is_block_exposed(v):
    return Utils.is_block_exposed(terrain, v)

