const Vox3D = preload("./Vox3D.gd")
const Consts = preload('./Consts.gd')
const Checkers = preload('./CheckerFunctions.gd')
const EMPTY = Consts.EMPTY

# Enumerated based on "filled" edge
const FIRST_RAMP  = Consts.RAMP_1
const LAST_RAMP  = Consts.RAMP_12

var vox3d

func _init(max_x, max_y, max_z):
    vox3d = Vox3D.new(max_x, max_y, max_z)

func set_out_of_bounds(value, warn):
    vox3d.set_out_of_bounds(value, warn)

#
# Init block to given shape
func init_block(x, y, z, shape):
    vox3d.set(x, y, z, shape)

#
# Boolean check that uses collision shapes, for physics collision.
func check(x, y, z):
    # TODO: Consistent system for setting out of bounds flag and
    # checking errors
    var value = vox3d.get(x, y, z)

    if value == EMPTY: # Check for a completely empty block
        return false

    #### TERRAIN CHECK
    # Check all known block value
    if value == Consts.CUBE:
        return true
    elif value == Consts.SPHERE:
        if Checkers.check_sphere(x, y, z):
            return true
    elif value >= FIRST_RAMP and value <= LAST_RAMP:
        if Checkers.check_ramp(x, y, z, value):
            return true
    else:
        print("ShapedVox3D: WARNING: Unknown collision shape ID: " + str(value))
        return false

    # We reach here if it fails with a shape
    return false

#
# Same as Vox3D, except just takes a shaped vox3d
func get_index_array_with_offset(other_shapedv3d, x, y, z):
    return vox3d.get_index_array_with_offset(other_shapedv3d.vox3d, x, y, z)

#
# Simply wraps Vox3D
func clear():
    vox3d.clear()

func copy_from(raw_array):
    vox3d.copy_from(raw_array)

#
# Simply returns the value at location by the vector (DEPRECATED)
func get_by_vector(v):
    return vox3d.get(v.x, v.y, v.z)

#
# Traces a ray starting at v3 going in direction v3, and returns the
# first collision. Very slowly / naively implemented. Granular to 1/2
# block.
const MAX_TRIES = 500
func trace_ray_collision(origin_v3, direction_v3):
    var increment = direction_v3.normalized() / Vector3(2, 2, 2)
    var loc = origin_v3

    # TODO: naive, should handle origins that are very much out of
    # bounds, can handle it by first projecting into bounds, then
    # looping for collisions. Should stop checking when out of bounds
    var tries_left = MAX_TRIES
    while not check(loc.x, loc.y, loc.z) and not tries_left < 0:
        tries_left -= 1
        loc += increment # move in direction of ray
    if not vox3d.vector_in_bounds(loc):
        return null
    return loc.floor()

