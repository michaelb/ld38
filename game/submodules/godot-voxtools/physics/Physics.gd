# TODO: In heavy development

const Vox3D = preload('../Vox3D.gd')
const DConsts = preload('../Consts.gd')
const COLLISION_EDGE = 0.1
var hard_floor = 1
var gravity = 20
var friction = 5
var stopping_friction = 20

const EMPTY = 0
const CUBE = 1
const ENTITY = 127

var blocks
var collision_3d
#var activators_3d

# Map block indices to entity lists
var collideable_by_index = {}

var world = null

# NOTE: Until we have Vox3D etc sorted out, we just do this:
var activators_by_index = {}

func _init(world):
    reload_from_world(world)

func reload_from_world(world):
    self.world = world
    var vector = world.get_world_dimension_vector()
    #activators_3d = Dict3D.new(vector.x, vector.y, vector.z)
    collision_3d = Vox3D.new(vector.x, vector.y, vector.z)
    collision_3d.copy_from(world.shape_vox3d.vox3d.data)
    blocks = collision_3d.data # temporary

func _get_index(pos):
    return collision_3d.index_of(pos.x, pos.y, pos.z)

func lock_pos_to_floor(physicable, pos):
    var floor_pos = (pos + physicable.foot_offset).floor()
    floor_pos.y = int(floor_pos.y - COLLISION_EDGE)
    var i = _get_index(floor_pos)

    # NOTE: should disable for physics debugging
    if floor_pos.y < hard_floor:
        # Add in hard floor at y = 0
        pos.y = hard_floor # NOTE, maybe cannot snap, because vectors are passed by value
        physicable.base_vel = Vector3() # standing on hard floor, reset base vel
        return true

    if blocks[i] == ENTITY:
        # Standing on top of an entity, we need to see if this one can move
        var entity = _get_collision_entity_for_index(i)
        if entity and entity.vel != Vector3():
            # If entity exists and has velocity, set this as the base_vel
            physicable.base_vel = entity.vel
        return true

    if blocks[i] != EMPTY:
        pos.y = floor_pos.y # Snap into place
        physicable.base_vel = Vector3() # standing on non-moving floor, reset base vel
        return true
    else:
        return false

func _get_collision_entity_for_index(i):
    if collideable_by_index.has(i):
        if collideable_by_index[i] == null:
            print("WARNING: Collision entity at " + str(i) + " is null.")
            return null
    return collideable_by_index[i]


###################################
# process
#
# Take a Physicable that has no impulse energy associated (e.g., a non-moving
# fixture or bullet) and applies physics to it. Provides simplified interface
# compared to process_impulse.
###################################
func process(physicable, delta):
    var pos = physicable.get_translation()

    var horizontal_vel = Vector3(physicable.vel.x, 0, physicable.vel.z)

    # If has friction, apply friction resisting current movement
    if physicable.has_friction:
        horizontal_vel += -(horizontal_vel * friction * delta)

    # Add horizontal speed limit
    var speed = clamp(horizontal_vel.length(), -physicable.max_speed, physicable.max_speed)
    horizontal_vel = horizontal_vel.normalized() * speed
    physicable.vel.x = horizontal_vel.x
    physicable.vel.z = horizontal_vel.z

    # Check if floor (while snapping to floor)
    if physicable.has_gravity:
        physicable.state_is_on_floor = lock_pos_to_floor(physicable, pos)
        if physicable.state_is_on_floor:
            physicable.vel.y = 0
        else:
            # not on floor, and gravity affects this, apply falling
            physicable.vel.y -= gravity * delta # apply gravity to vel

    # Now, check and slide along each direction:
    var moment = physicable.vel * delta + (physicable.base_vel * delta)
    pos = check_and_slide_all_axis(physicable, pos, moment)

    # Update location
    physicable.physicable_move_to(pos)
    # check_activators(pos, physicable) not used


###################################
# process_impulse
#
# Take a Physicable that is attempting to move (e.g. player character, or mob)
# and applies physics to it.
###################################
func process_impulse(physicable, walk_impulse, jump_impulse, delta):
    var pos = physicable.get_translation()

    var walk_velocity_delta
    var horizontal_vel = Vector3(physicable.vel.x, 0, physicable.vel.z)
    var acceleration = physicable.acceleration
    if walk_impulse.distance_to(horizontal_vel) > 1.5: # changing directions
        acceleration = physicable.pivot_acceleration # acceleration boost when changing dirs
    if walk_impulse != Vector3():
        # Intentional movement, apply acceleration
        walk_velocity_delta = walk_impulse * acceleration * delta
    else:
        # No intentional movement, apply stopping friction
        walk_velocity_delta = -horizontal_vel * stopping_friction * delta

    # If not flying, always apply friction resisting current movement
    walk_velocity_delta = walk_velocity_delta - (horizontal_vel * friction * delta)
    horizontal_vel += walk_velocity_delta

    # Add horizontal speed limit
    var speed = clamp(horizontal_vel.length(), -physicable.max_speed, physicable.max_speed)
    horizontal_vel = horizontal_vel.normalized() * speed
    physicable.vel.x = horizontal_vel.x
    physicable.vel.z = horizontal_vel.z

    # Check if floor (while snapping to floor)
    physicable.state_is_on_floor = lock_pos_to_floor(physicable, pos)

    if physicable.state_is_on_floor:
        # On floor, lets apply jump impulse + clear falling
        physicable.vel.y = jump_impulse
    elif physicable.has_gravity:
        # not on floor, and gravity affects this, apply falling
        physicable.vel.y -= gravity * delta # apply gravity to vel

    # Now, check and slide along each direction:
    var moment = physicable.vel * delta + (physicable.base_vel * delta)
    pos = check_and_slide_all_axis(physicable, pos, moment)

    # Update location
    physicable.set_translation(pos)
    check_activators(pos, physicable)

func check_extents(pos, extents):
    for extent in extents:
        var location = (pos + extent).floor()
        var i = _get_index(location)
        if i >= blocks.size() or i < 0:
            print("Warning: Colliding out of bounds!")
            return CUBE # out of bounds, always collide
        if blocks[i] != EMPTY:
            return blocks[i]
    return EMPTY

func get_collision_entity(pos, extents):
    for extent in extents:
        var location = (pos + extent).floor()
        var i = _get_index(location)
        if blocks[i] == ENTITY:
            if collideable_by_index.has(i):
                if collideable_by_index[i] == null:
                    print("WARNING: Collision entity at " + str(location) + " is null.")
                    continue
                return collideable_by_index[i]
    print("WARNING: Could not find collision entity for " + str(pos))
    return null

func check_and_slide_all_axis(physicable, pos, moment):
    var extents = physicable.extents

    if check_extents(pos, extents) == CUBE:
        # NOTE: Does not check for being "stuck" in ENTITY
        # within block, TODO, have pop-out safety algo
        return pos + moment # no collisions within block

    var collision_type = check_extents(pos + moment, extents)
    if collision_type == EMPTY:
        physicable.state_is_colliding = false
        return pos + moment # no collision

    if physicable.is_collidable:
        # presently, collidable ENTITY can't collide with other collidables
        if collision_type == ENTITY:
            physicable.state_is_colliding = false
            return pos + moment # no collision, slide along

    # If the physicable uses collision info, then set all those props
    physicable.state_is_colliding = true
    if physicable.uses_collision_meta:
        physicable.state_last_collision_location = (pos + moment)
        physicable.state_last_collision_type = collision_type
        if collision_type == ENTITY:
            physicable.state_last_collision_entity = get_collision_entity(pos + moment, extents)

    if not physicable.has_sliding:
        return pos # no 'sliding' movement if sliding is disabled

    # Try a few different ways
    # TODO Really messy algo here, needs clean up + optimize
    # TODO: fully stop velocity if its going in some direction
    var attempts = 6

    #var moment_y = max(0, moment.y)
    for attempt in range(attempts):
        var new_moment = Vector3(moment.x, moment.y, moment.z)
        # Try 1 at once
        if attempt == 0:
            new_moment.x = 0
        elif attempt == 1:
            new_moment.y = 0
        elif attempt == 2:
            new_moment.z = 0

        # Try 2 combos
        elif attempt == 3:
            new_moment.y = 0
            new_moment.x = 0
        elif attempt == 4:
            new_moment.y = 0
            new_moment.z = 0
        elif attempt == 5:
            new_moment.x = 0
            new_moment.z = 0

        if check_extents(pos + new_moment, extents) == EMPTY:
            return pos + new_moment

    # This means all movement is rejected, return original position
    return pos


# For a given location and physicable, it checks all extents for collisions
# with activatable entities
# NOTE: Eventually this should get replaced with the Dict3D system
func check_activators(pos, entity):
    for extent in entity.extents:
        var location = (pos + extent).floor()
        var i = _get_index(location)
        if activators_by_index.has(i) and activators_by_index[i] != null:
            var activator = activators_by_index[i]
            activator.entity_activated(entity, location)


#########################
# Adds the collide-able entity at the given location
func add_collideable(pos, entity):
    # For now, no partial-voxel collisions possible, eventually can be
    # implemented with different collision shapes
    # NOTE: also, importantly assumes no overlap
    var location = pos.floor()
    for offset in entity.collision_blocks:
        var i = _get_index(location + offset)
        collideable_by_index[i] = entity
        # Only overwrite empty blocks
        if blocks[i] == EMPTY:
            blocks.set(i, ENTITY)

#########################
# Removes the collideable entity from the given location
func remove_collideable(pos, entity):
    var location = pos.floor()
    for offset in entity.collision_blocks:
        # Only reset entity blocks
        var i = _get_index(location + offset)
        collideable_by_index[i] = null
        if blocks[i] == ENTITY:
            blocks.set(i, EMPTY)

#########################
# Adds activator trigger at given index
func add_activator(pos, entity):
    activators_by_index[_get_index(pos.floor())] = entity
func remove_activator(pos, entity):
    activators_by_index[_get_index(pos.floor())] = null
#########################

#########################
# After loaded, add an additional block
func add_block(location):
    blocks.set(_get_index(location.floor()), CUBE)
