const Consts = preload('../Consts.gd')
const Utils = preload('../Utils.gd')

# Physics properties (should be overwritten):
const max_speed = 5
const acceleration = 30
const pivot_acceleration = 60
const has_gravity = true # does gravity affect this
const has_friction = true # does friction apply?
const has_sliding = true # slides along surfaces upon impact
const is_collidable = false # does not occupy space
const uses_collision_meta = false # does not have special triggers that use collision meta
const foot_offset = Vector3(0, 0, 0)
const extents = [Vector3(0, 0, 0)]

var collision_dict = Utils.from_array(Consts.CUBE, [[1, 1, 1]])

# Used for certain collisions (NOTE: Temporary until we get universal
# Dict3D implemented)
var is_activatable = true

# Physics state:
const COLLISION_BLOCK = 1
const COLLISION_MOB = 2
const COLLISION_FIXTURE = 3

var vel = Vector3()

# If standing on a moving platform, base velocity is non-zero
var base_vel = Vector3(0, 0, 0)

var state_is_colliding = false
var state_is_on_floor = false
# var state_is_on_fixture = false <-- TODO for moving platforms
var state_last_collision_type = null
var state_last_collision_location = null
var state_last_collision_entity = null


# World pointers:
var physics # physics simulator
var world # world that its in
func add_to(world, physics):
    self.world = world
    self.physics = physics

func throw_projectile(bullet, direction):
    var my_pos = self.get_translation()
    bullet.add_to(world, physics)
    get_parent().add_child(bullet)
    bullet.set_translation(self.get_translation() + Vector3(0, 1, 0) + direction)
    bullet.vel = direction * 10

# Sets the translation of this object.
# If its collide able, also manage moving the collision voxels to the right
# location
func physicable_move_to(vector):
    var my_pos = get_translation()
    set_translation(vector)
    if not is_collidable:
        return
    if is_activatable:
        # NOTE: replace this with generalized collisions with Dict3D
        physics.remove_activator(my_pos, self)
        physics.add_activator(vector, self)

    if my_pos.floor() != vector.floor():
        # Moving low-res location, also: trigger physics engine update
        physics.remove_collideable(my_pos, self)
        physics.add_collideable(vector, self)

func physicable_delete():
    var my_pos = get_translation()
    physics.remove_collideable(my_pos, self)
    if is_activatable:
        physics.remove_activator(my_pos, self)

