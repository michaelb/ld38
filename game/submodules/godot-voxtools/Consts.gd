const EMPTY = 0
const CUBE = 1
const SPHERE = 2 # unit sphere

const RAMP_1  = 16
const RAMP_2  = 17
const RAMP_3  = 18
const RAMP_4  = 19
const RAMP_5  = 20
const RAMP_6  = 21
const RAMP_7  = 22
const RAMP_8  = 23
const RAMP_9  = 24
const RAMP_10 = 25
const RAMP_11 = 26
const RAMP_12 = 27

const ENTITY = 127

# Single plane type collides
const PLANES = {
    # Enumerating edges of a cube:
    #         2
    #     ___________
    #  1 /.       3 /|      ^ y
    #   /_________ / |      :
    #  |  . 4     |  | 7    :
    #  | 6.      8|  |      :
    # 5|  .. . . .|. |      o------- > x
    #  | '9  10   | /     .'
    #  |'_________|/11  L'  z
    #       12        
    # For the ramp enumerations, the Cube edge that is fully contained
    # in the ramp (that is, the cube edge that forms a 90deg corner)
    # gives the ramp its number.

    # Can specify via normal and D value
    CUBE:    Plane(Vector3( 0,  1,  0),   1),
    RAMP_1:  Plane(Vector3( 1, -1,  0),   0),
    RAMP_2:  Plane(Vector3( 0, -1,  1),   0),
    RAMP_3:  Plane(Vector3(-1, -1,  0),  -0.5), # X
    RAMP_4:  Plane(Vector3( 0, -1, -1),  -0.5), # X
    RAMP_5:  Plane(Vector3( 1,  0, -1),   0),
    RAMP_6:  Plane(Vector3( 1,  0,  1),   0.5), # X
    RAMP_7:  Plane(Vector3(-1,  0,  1),   0),
    RAMP_8:  Plane(Vector3(-1,  0, -1),  -0.5), # X
    RAMP_9:  Plane(Vector3( 1,  1,  0),   0.5), # X
    RAMP_10: Plane(Vector3( 0,  1,  1),   0.5), # X
    RAMP_11: Plane(Vector3(-1,  1,  0),   0),
    RAMP_12: Plane(Vector3( 0,  1, -1),   0),
}

const COMPLIMENTS = {
    CUBE:    EMPTY,
    EMPTY:   CUBE,
    RAMP_1:  RAMP_11,
    RAMP_2:  RAMP_12,
    RAMP_3:  RAMP_9,
    RAMP_4:  RAMP_10,
    RAMP_5:  RAMP_7,
    RAMP_6:  RAMP_8,
    RAMP_7:  RAMP_5,
    RAMP_8:  RAMP_6,
    RAMP_9:  RAMP_3,
    RAMP_10: RAMP_4,
    RAMP_11: RAMP_1,
    RAMP_12: RAMP_2,
}


# Bit side encoding
#const X_POS = 0b00000001
#const X_NEG = 0b00000010
#const Y_POS = 0b00000100
#const Y_NEG = 0b00001000
#const Z_POS = 0b00010000
#const Z_NEG = 0b00100000


