# Essentially tests both Vox3D and ShapedVox3D

extends "./unittest.gd"

const ShapedVox3D = preload("../ShapedVox3D.gd")
const Consts = preload("../Consts.gd")
const Utils = preload("../Utils.gd")
const CheckerFunctions = preload("../CheckerFunctions.gd")

const ENT_PLACEHOLDER = 'some test data'

var d3d
var entity_d3d

func tests():
    _tests_basic_tests()
    _tests_get_index_array_with_offset()
    _tests_instantiation_helpers()
    _tests_ray_collision()

func _tests_basic_tests():
    testcase('boundaries and initial state, 1x1x1')
    var sv3d
    sv3d = ShapedVox3D.new(1, 1, 1)
    sv3d.set_out_of_bounds(Consts.EMPTY, false)
    assert_false(sv3d.check(0, 0, 0), 'in bounds')
    assert_false(sv3d.check(0, 0, -1), 'out of bounds empty (and board empty) (before)')
    assert_false(sv3d.check(0, 0, 1), 'out of bounds empty (and board empty) (after)')
    sv3d.set_out_of_bounds(Consts.CUBE, false)
    assert_false(sv3d.check(0, 0, 0), 'in bounds')
    assert_true(sv3d.check(-1, 0, 0), 'out of bounds filled (before)')
    assert_true(sv3d.check(1, 0, 0), 'out of bounds filled (after)')
    sv3d.init_block(0, 0, 0, Consts.CUBE)
    sv3d.set_out_of_bounds(Consts.EMPTY, false)
    assert_true(sv3d.check(0, 0, 0), 'in bounds')
    assert_false(sv3d.check(0, 0, -1), 'out of bounds empty (before)')
    assert_false(sv3d.check(0, 0, 1), 'out of bounds empty (after)')
    assert_eq(sv3d.vox3d.get_by_vector(Vector3(0, 0, 0)), Consts.CUBE, 'gets cube value')
    endcase()

    testcase('random testing, different max values, 1x2x3')
    # Fills a few arbitrary blocks and checks against 'em
    sv3d = ShapedVox3D.new(1, 2, 3)
    sv3d.init_block(0, 0, 0, Consts.CUBE)
    sv3d.init_block(0, 0, 1, Consts.CUBE)
    sv3d.init_block(0, 1, 1, Consts.CUBE)
    sv3d.init_block(0, 1, 2, Consts.CUBE)
    for x in range(1):
        for y in range(2):
            for z in range(3):
                var v = Vector3(x, y, z)
                var b1 = v == Vector3(0, 0, 0) or v == Vector3(0, 0, 1)
                if b1 or v == Vector3(0, 1, 1) or v == Vector3(0, 1, 2):
                    assert_true(sv3d.check(x, y, z), 'in bounds and true ' + str(v))
                else:
                    assert_false(sv3d.check(x, y, z), 'in bounds and false ' + str(v))
    endcase()

    testcase('some random testing using sphere and cube 1x2x1')
    sv3d = ShapedVox3D.new(1, 2, 1)
    sv3d.init_block(0, 0, 0, Consts.CUBE)
    sv3d.init_block(0, 1, 0, Consts.SPHERE)
    assert_true(sv3d.check(0, 0, 0), 'cube checks out (1)')
    assert_true(sv3d.check(0.5, 0.5, 0.5), 'cube checks out (2)')
    assert_true(sv3d.check(0.9, 0.9, 0.9), 'cube checks out (3)')
    assert_false(sv3d.check(0, 1, 0), 'sphere checks out (1)')
    assert_true(sv3d.check(0.5, 1.5, 0.5), 'cube checks out (2)')
    assert_false(sv3d.check(0.9, 1.9, 0.9), 'sphere checks out (2)')
    endcase()

func _tests_get_index_array_with_offset():
    testcase('method get_index_array_with_offset')
    d3d = ShapedVox3D.new(10, 10, 10)
    entity_d3d = ShapedVox3D.new(5, 5, 5)
    var index_array = entity_d3d.get_index_array_with_offset(d3d, 0, 0, 0)
    var expected_array = IntArray()
    for y in range(5):
        for x in range(5):
            for z in range(5):
                expected_array.append(x + z * 10 + y * 10 * 10)
    assert_array_equal(index_array, expected_array, "with 0 offset")

    index_array = entity_d3d.get_index_array_with_offset(d3d, 3, 2, 1)
    expected_array = IntArray()
    for y in range(2, 2 + 5):
        for x in range(3, 3 + 5):
            for z in range(1, 1 + 5):
                expected_array.append(x + z * 10 + y * 10 * 10)
    assert_array_equal(index_array, expected_array, "with 3,2,1 offset")

    index_array = entity_d3d.get_index_array_with_offset(d3d, 3.1, 2.0, 1.0)
    expected_array = IntArray()
    for y in range(2, 2 + 5):
        for x in range(3, 3 + 6):
            for z in range(1, 1 + 5):
                expected_array.append(x + z * 10 + y * 10 * 10)
    assert_array_equal(index_array, expected_array, "with uneven 3.1, 2.0, 1.0 offset")

    index_array = entity_d3d.get_index_array_with_offset(d3d, 3.1, 2.0, 1.1)
    expected_array = IntArray()
    for y in range(2, 2 + 5):
        for x in range(3, 3 + 6):
            for z in range(1, 1 + 6):
                expected_array.append(x + z * 10 + y * 10 * 10)
    assert_array_equal(index_array, expected_array, "with uneven 3.1, 2.0, 1.1 offset")

    entity_d3d = ShapedVox3D.new(1, 2, 3)
    index_array = entity_d3d.get_index_array_with_offset(d3d, 3, 2, 1)
    expected_array = IntArray()
    for y in range(2, 2 + 2):
        for x in range(3, 3 + 1):
            for z in range(1, 1 + 3):
                expected_array.append(x + z * 10 + y * 10 * 10)
    assert_array_equal(index_array, expected_array, "with 3,2,1 offset")
    endcase()

func _tests_instantiation_helpers():
    testcase('instantiate a single block from array')
    d3d = Utils.from_v3_array(Consts.CUBE, [Vector3(0, 0, 0)])
    assert_eq(d3d.vox3d.max_x, 1, 'correct dimensions (x)')
    assert_eq(d3d.vox3d.max_y, 1, 'correct dimensions (y)')
    assert_eq(d3d.vox3d.max_z, 1, 'correct dimensions (z)')
    assert_true(d3d.check(0, 0, 0), 'in bounds')
    endcase()

    testcase('instantiate a more complicated shape from array')
    var arr = [[0, 0, 0], [0, 0, 1], [0, 1, 1], [0, 1, 2]]
    d3d = Utils.from_array(Consts.CUBE, arr)
    assert_eq(d3d.vox3d.max_x, 1, 'correct dimensions (x)')
    assert_eq(d3d.vox3d.max_y, 2, 'correct dimensions (y)')
    assert_eq(d3d.vox3d.max_z, 3, 'correct dimensions (z)')
    for x in range(1):
        for y in range(2):
            for z in range(3):
                var v = Vector3(x, y, z)
                var b1 = v == Vector3(0, 0, 0) or v == Vector3(0, 0, 1)
                if b1 or v == Vector3(0, 1, 1) or v == Vector3(0, 1, 2):
                    assert_true(d3d.check(x, y, z), 'in bounds and true ' + str(v))
                else:
                    assert_false(d3d.check(x, y, z), 'in bounds and false ' + str(v))
    endcase()

    testcase('instantiates a 1x2x1 using cube and sphere')
    var arr = [[0, 0, 0], [0, 0, 1], [0, 1, 1], [0, 1, 2]]
    d3d = Utils.from_v3_dict({
        Vector3(0, 0, 0): Consts.CUBE,
        Vector3(0, 1, 0): Consts.SPHERE
    })
    assert_true(d3d.check(0, 0, 0), 'cube checks out (1)')
    assert_true(d3d.check(0.5, 0.5, 0.5), 'cube checks out (2)')
    assert_true(d3d.check(0.9, 0.9, 0.9), 'cube checks out (3)')
    assert_false(d3d.check(0, 1, 0), 'sphere checks out (1)')
    assert_true(d3d.check(0.5, 1.5, 0.5), 'cube checks out (2)')
    assert_false(d3d.check(0.9, 1.9, 0.9), 'sphere checks out (2)')
    endcase()

func _tests_ray_collision():
    var col
    testcase('ray can pick terrain in 5x5x5')
    d3d = ShapedVox3D.new(5, 5, 5)
    d3d.set_out_of_bounds(Consts.EMPTY, false) # ensure out of bounds is empty
    d3d.init_block(1, 1, 1, Consts.CUBE)
    d3d.init_block(0, 0, 0, Consts.CUBE)
    d3d.init_block(3, 4, 4, Consts.CUBE)
    col = d3d.trace_ray_collision(Vector3(5, 5, 5), Vector3(-1, -1, -1))
    assert_eq(col, Vector3(1, 1, 1), 'collided at angle (1)')
    col = d3d.trace_ray_collision(Vector3(1, 5, 5), Vector3(0, -1, -1))
    assert_eq(col, Vector3(1, 1, 1), 'collided at angle (2)')
    col = d3d.trace_ray_collision(Vector3(0, 0, 10), Vector3(0, 0, -1))
    assert_eq(col, Vector3(0, 0, 0), 'collided at angle (3)')
    col = d3d.trace_ray_collision(Vector3(2, 2, 2), Vector3(1, 1.5, 1.5))
    assert_eq(col, Vector3(3, 4, 4), 'collided at angle (4)')

    # should totally miss
    col = d3d.trace_ray_collision(Vector3(2, 2, 2), Vector3(1, 1, 1))
    assert_eq(col, null, 'no collision at angle (1)')
    col = d3d.trace_ray_collision(Vector3(2, 2, 2), Vector3(1, 0, 0))
    assert_eq(col, null, 'no collision at angle (2)')
    col = d3d.trace_ray_collision(Vector3(2, 2, 2), Vector3(0, 1, 0))
    assert_eq(col, null, 'no collision at angle (3)')
    endcase()
