extends "./unittest.gd"

const EntityShapedVox3D = preload("../EntityShapedVox3D.gd")
const ShapedVox3D = preload("../ShapedVox3D.gd")
const Consts = preload("../Consts.gd")

const ENT_PLACEHOLDER = 'some test data'

var d3d
var entity_d3d

func tests():
    _tests_entity_checks()
    _tests_entity_removal()

func _tests_entity_checks():
    var expected
    testcase('entity gets added to expected dicts, 5x5x5')
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(1, 1, 1)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    var entity_id = d3d.add_entity(1, 2, 3, entity_d3d, ENT_PLACEHOLDER)
    assert_gt(entity_id, 0, 'entity id is valid and truthy')

    # now lets do some white box unit testing for sanity's sake
    expected = {d3d.vox3d.index_of(1, 2, 3): [entity_id]}
    assert_dict_equal(expected, d3d.entities_by_block_index, 'entity_id added to block index')
    expected = {entity_id: ENT_PLACEHOLDER}
    assert_dict_equal(expected, d3d.entity_by_id, 'entity_id added to id index')
    expected = {entity_id: entity_d3d}
    assert_dict_equal(expected, d3d.entity_collision_dicts, 'collision index added to dict')
    expected = {entity_id: Vector3(1, 2, 3)}
    assert_dict_equal(expected, d3d.entity_locations, 'entity precise location noted')
    endcase()

    testcase('entity check works correctly with snapped block')
    assert_true(d3d.check(1.1, 2.1, 3.1), 'odd numbers work')
    assert_false(d3d.check(2.1, 2.1, 3.1), 'odd numbers work')
    for x in range(5):
        for y in range(5):
            for z in range(5):
                var v = Vector3(x, y, z)
                if v == Vector3(1, 2, 3):
                    assert_true(d3d.check(x, y, z), 'in bounds and true ' + str(v))
                else:
                    assert_false(d3d.check(x, y, z), 'in bounds and false ' + str(v))
    endcase()

    testcase('entity check works correctly when added to non-snapped block')
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(1, 1, 1)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    d3d.add_entity(1.1, 2.1, 3.1, entity_d3d, ENT_PLACEHOLDER)

    # TODO: for some reason, it is testing entity_d3d out of bounds, might be a bug
    entity_d3d.set_out_of_bounds(0, false)

    # Now check in and out of bounds
    assert_true(d3d.check(1.1, 2.1, 3.1), 'exact number works')
    assert_false(d3d.check(1, 2, 3), 'a little out of bounds')
    assert_true(d3d.check(2, 3, 4), 'just in bounds, in the other corner')
    assert_true(d3d.check(2.1, 2.1, 3.1), 'on the top edge')
    assert_false(d3d.check(2.0, 2.0, 3.0), 'just outside')
    endcase()

    testcase('entity moving from snapped to non-snapped block works')
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(1, 1, 1)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    entity_id = d3d.add_entity(1, 2, 3, entity_d3d, ENT_PLACEHOLDER)
    d3d.update_entity_location(entity_id, 1.1, 2.1, 3.1)

    # TODO: for some reason, it is testing entity_d3d out of bounds, might be a bug
    entity_d3d.set_out_of_bounds(0, false)

    # Exact same checks as before
    assert_true(d3d.check(1.1, 2.1, 3.1), 'exact number works')
    assert_false(d3d.check(1, 2, 3), 'a little out of bounds')
    assert_true(d3d.check(2, 3, 4), 'just in bounds, in the other corner')
    assert_true(d3d.check(2.1, 2.1, 3.1), 'on the top edge')
    assert_false(d3d.check(2.0, 2.0, 3.0), 'just outside')
    endcase()

    testcase('entity checks with various sizes at 1x2x3')
    # Fills a few arbitrary blocks and checks against 'em
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(1, 2, 3)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    entity_d3d.init_block(0, 0, 1, Consts.CUBE)
    entity_d3d.init_block(0, 1, 1, Consts.CUBE)
    entity_d3d.init_block(0, 1, 2, Consts.CUBE)
    d3d.add_entity(2, 1, 0, entity_d3d, ENT_PLACEHOLDER)
    for x in range(5):
        for y in range(5):
            for z in range(5):
                var v = Vector3(x, y, z) - Vector3(2, 1, 0)
                var b1 = v == Vector3(0, 0, 0) or v == Vector3(0, 0, 1)
                if b1 or v == Vector3(0, 1, 1) or v == Vector3(0, 1, 2):
                    assert_true(d3d.check(x, y, z), 'in bounds and true ' + str(v))
                else:
                    assert_false(d3d.check(x, y, z), 'in bounds and false ' + str(v))
    endcase()


func _tests_entity_removal():
    testcase('entity removal works as expected')
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(1, 1, 1)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    var entity_id = d3d.add_entity(1.1, 2.1, 3.1, entity_d3d, ENT_PLACEHOLDER)
    d3d.remove_entity(entity_id)

    # Now check everywhere is false
    assert_false(d3d.check(1.1, 2.1, 3.1), 'exact number works')
    assert_false(d3d.check(2.1, 2.1, 3.1), 'on the top edge')
    for x in range(5):
        for y in range(5):
            for z in range(5):
                var v = Vector3(x, y, z)
                assert_false(d3d.check(x, y, z), 'always false ' + str(v))
    endcase()

    testcase('large entity removal works as expected')
    d3d = EntityShapedVox3D.new(5, 5, 5)
    entity_d3d = ShapedVox3D.new(3, 3, 3)
    entity_d3d.init_block(0, 0, 0, Consts.CUBE)
    entity_d3d.init_block(1, 1, 1, Consts.CUBE)
    entity_d3d.init_block(2, 2, 2, Consts.CUBE)
    entity_d3d.init_block(1, 1, 2, Consts.CUBE)
    entity_d3d.init_block(0, 1, 2, Consts.CUBE)
    entity_id = d3d.add_entity(1.1, 2.1, 3.1, entity_d3d, ENT_PLACEHOLDER)
    d3d.remove_entity(entity_id)
    for x in range(5):
        for y in range(5):
            for z in range(5):
                var v = Vector3(x, y, z)
                assert_false(d3d.check(x, y, z), 'always false ' + str(v))

    var expected = {}
    # Ensure properly cleaned up, e.g. no memory leaks etc
    for value in d3d.entities_by_block_index.values():
        assert_array_equal(value, [], 'any remaining lists are empty')
    assert_dict_equal(expected, d3d.entity_by_id, 'entity_id added to id index')
    assert_dict_equal(expected, d3d.entity_collision_dicts, 'collision index added to dict')
    assert_dict_equal(expected, d3d.entity_locations, 'entity precise location noted')
    endcase()
