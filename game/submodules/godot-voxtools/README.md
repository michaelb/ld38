# godot-voxtools

**Miscellaneous voxel and block-based platformer game related tools.**

This is a library I'm developing as I work on some voxel-based games
with the FOSS [Godot Game Engine](http://godotengine.org/).

It can also be used for 2D platformers, or anything with "block-y" type
terrain. It provides a datatype for storing and retrieving volumetric
data composed of simple block types (cubes, ramps, etc), and performs
well for data that is mostly cubes with few mobile entities, e.g. a 2D
or 3D platformer, or infiniminer / minecraft like voxel game.

### Rational

I developed it since built-in physics and collision detection
would be as much work to get to do what I wanted it to do as to
implement a custom system. Thus, it should be useful for people
interested in hand-writing platformer physics engines.

It's very much WIP, so documentation at the moment will be sparse, but
I'll update it and polish it up as it becomes more usable. So, poke
around, but don't expect a stable or well-thought-out interface. Also,
it may re-invent built-in features, I had very particular uses for it,
so you may be better off just using the built-in physics and collision
detection tools available.

Presently, it's entirely implemented in GDScript, which is slow
(although fast enough for my purposes). Eventually I may consider a C++
port with the same interface if speed becomes limiting.

### Usage

Personally, I check out libraries as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

To run the test suite, run `godot-server -s tests/runtests.gd`.
(Assuming `godot-server` is the headless godot client.) You should see
`[SUCCESS]` printed if all goes well.

# Library documentation

## ShapedVox3D

This is the main class for detecting collisions in a block-y 3D space.
Allocates a linear byte-array of `x * y * z` dimensions.  Could be used
to represent a single "chunk" in a procedural game, or an entire
(small-ish) level in a non-procedural platformer.

### Building voxel data

* `func _init(max_x, max_y, max_z)` -- instantiate with fixed boundaries.

* `func init_block(x, y, z, shape)` -- Init block to given shape,
  clearing ENTITY bit if necessary (possibly orphaning entities).
  Coordinates should be ints.

* `func set_block(x, y, z, shape)` -- Like init, but maintains ENTITY
  bit.  Coordinates should be ints.

### Checking collisions

* `func check(x, y, z)` -- Returns true if there is either voxel
  (terrain) data there, or an entity collision.  Coordinates can be
  floats.

### Entity management

Entities are movable collideable objects, and useful for things like
moving platforms, doors, or player characters or mobs (if you want to
permit "stacking").

Use `EntityShapedVox3D` for this.

* `func add_entity(x, y, z, collision_dict, entity) -> (int) entity_id`
  -- Adds a movable entity, which itself has a shape specified by
  another ShapedVox3D (`collision_dict`). "Entity" can be any sort of
  object, such as a `Node`. Coordinates can be floats.

* `func update_entity_location(entity_id, x, y, z)` -- moves entity to
  new location. Coordinates can be floats.

* `func remove_entity(entity_id)` -- removes given entity

* `func get_entities(x, y, z)` -- gets an array of all entities that exist at
  given location. Coordinates can be floats.



