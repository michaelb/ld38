
const Consts = preload("./Consts.gd")

static func _get_vector(x, y, z):
    var v = Vector3(x, y, z)
    return v - v.floor()

const SPHERE_R2 = 0.25 # unit sphere radius squared
static func check_sphere(x, y, z):
    var v = _get_vector(x, y, z)
    return v.distance_squared_to(Vector3(0.5, 0.5, 0.5)) <= SPHERE_R2

static func check_ramp(x, y, z, ramp_number):
    # Simply checks if the point is "above" (e.g., in the direction of
    # the normal) of the appropriate Plane
    #var v = _get_vector(x, y, z) # For some reason this is broken
    var v = Vector3(x, y, z)
    return not Consts.PLANES[ramp_number].is_point_over(v)
