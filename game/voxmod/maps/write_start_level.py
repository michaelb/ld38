import sys
size = int(sys.argv[1])
dest = sys.argv[2]

if not size or not dest:
    print 'whoops'
    sys.exit(0)

SIZE = size

#flat_b_row = ([1] * 16) + ([0] * 15 * 16)
flat_b_row = ([1] * SIZE * SIZE)

flat_bottom = (
    flat_b_row
    +
    [0] * (SIZE - 1)  * SIZE * SIZE
)

newfile = open(dest, 'wb')
newfile.write(bytearray(flat_bottom))

newfile = open(dest + '.shape', 'wb')
newfile.write(bytearray(flat_bottom))
