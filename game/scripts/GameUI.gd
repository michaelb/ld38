const Cursor = preload('res://scenes/Cursor.tscn')

var game
var cursor = null
onready var sample_player = get_node('SamplePlayer')
var started = false
var voxel_terrain = null
var undoable = {}
var items_by_type = {}
func _ready():
    # set up reverse index for Items
    var items = get_node('Items').get_children()
    for item in items:
        items_by_type[item.code] = item

func set_game(game):
    self.game = game

func set_terrain(voxel_terrain, camera):
    if cursor != null:
        cursor.queue_free()
    cursor = Cursor.instance()
    cursor.set_camera(camera)
    self.voxel_terrain = voxel_terrain
    cursor.add_to_terrain(self, voxel_terrain)
    get_parent().add_child(cursor)

const BLOCK = 1
const DELETE = 2

const ARROW_BLOCK_L = 10 # <
const ARROW_BLOCK_U = 11 # ^
const ARROW_BLOCK_R = 12 # >
const ARROW_BLOCK_D = 13 # v

func attempt_action(location):
    if current_item == 0:
        return false # Nothing selected
    var item = items_by_type[current_item] # decrement items
    if item.count <= 0: # nothing left
        return false
    if current_item == BLOCK:
        voxel_terrain.add_block(location, 1, 2)
        undoable[location] = true
    elif current_item == DELETE:
        var remove_location = location + Vector3(0, -1, 0)
        if remove_location.y == 0: # Never remove at y = 0
            return false
        if undoable.has(remove_location):
            attempt_undo(location)
            return true # Try undoing instead of costing a destructo

        print("Removing block at: ", remove_location)
        voxel_terrain.remove_block(remove_location)
    else:
        undoable[location] = true
        voxel_terrain.add_block(location, 1, current_item)
    item.decrement() # decrement items
    sample_player.play('croak')
    return true

func attempt_undo(location):
    return # TODO
    var remove_location = location + Vector3(0, -1, 0)
    if undoable.has(remove_location):
        var type = voxel_terrain.voldata.get_block_material(remove_location)
        voxel_terrain.remove_block(remove_location)

func _deselect_all():
    var sibs = get_node('Items').get_children()
    for sib in sibs:
        sib.set_selected(false)

var current_item = 0
func select_item(item):
    _deselect_all()
    item.set_selected(true)
    if item.count > 0:
        current_item = item.code
    else:
        current_item = 0
    cursor.set_cursor(current_item)


var spawner = null
func set_spawner(spawner):
    self.spawner = spawner
    self.spawner.connect('spawn', self, 'on_spawned')
    self.spawner.connect('ascend', self, 'on_ascended')
    self.spawner.connect('die', self, 'on_die')
    self.spawner.connect('rotate', self, 'on_rotate')

func set_count(new_count):
    #get_node('BlocklingsLeftLabel').set_text(str(new_count) + '/6')
    get_node('BlocklingsLeftIcon').set_frame(new_count)

func set_items(items):
    for type in items:
        items_by_type[type].set_count(items[type])

func on_spawned():
    sample_player.play('bloop')

var ascended = 0
func on_ascended():
    ascended += 1
    var c = ascended
    if c >= 1 and c <= 6:
        sample_player.play('flute' + str(c))
    set_count(c)
    game.check_for_win(c)

func on_die():
    sample_player.play('scream')
func on_rotate():
    sample_player.play('cha')

func _on_StartButton_pressed():
    if started:
        return
    game.start()
    started = true

func _on_Restart_button_down():
    undoable = {}
    sample_player.play('revcro')
    game.restart()
