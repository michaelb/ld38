
const GameScene = preload('res://scenes/Game.tscn')
const utils = preload('./utils.gd')
onready var anim = get_node('AnimationPlayer')

onready var current_game_wrapper = get_node('GameSpatial/CurrentGameWrapper')
onready var next_game_wrapper = get_node('GameSpatial/NextGameWrapper')
onready var camera = get_node('GameSpatial/PannableCamera')

var game_scene = null
var starting = false
func _ready():
    game_scene = GameScene.instance()
    game_scene.camera = camera
    game_scene.loader = self
    game_scene.current_level = 'menu'
    add_child(game_scene)
    get_node('MenuMusic').set_volume_db(0)
    get_node('MenuMusic').play()
    game_scene.start() # auto starts since menu
    anim.play_backwards('FadeAway')
    #anim.play_backwards('FadeOutMusic')

var IntroNode
var MusicNode
var current_number = 0
#var current_number = 6 # test 2
func next_level():
    current_number += 1
    load_level(current_number)

func _new_game(level):
    game_scene = GameScene.instance()
    game_scene.camera = camera
    game_scene.loader = self
    game_scene.current_level = level

func restart():
    game_scene.queue_free()
    _new_game('level_' + str(current_number))
    _start_game_3()

func load_level(number):
    current_number = number
    game_scene.queue_free()
    IntroNode = load('res://scenes/intros/Intro' + str(number) + '.tscn')
    MusicNode = load('res://scenes/intros/Music' + str(number) + '.tscn')

    _new_game('level_' + str(current_number))
    #game_scene = GameScene.instance()
    #game_scene.camera = camera
    #game_scene.loader = self
    #game_scene.current_level = 'level_' + str(number)
    utils.timeout(self, '_start_game_1', 2)
    anim.play('FadeAway')

var inode = null
func _start_game_1():
    if IntroNode == null:
        _start_game_3()
        return
    inode = IntroNode.instance()
    get_node('LevelMenu').add_child(inode)
    anim.play('FadeLevelIn')
    inode.get_node('StreamPlayer').play()
    utils.timeout(self, '_start_game_2', 10)

func _start_game_2():
    anim.play_backwards('FadeLevelIn')
    utils.timeout(self, '_start_game_3', 3)

func _start_game_3():
    anim.play('FadeOutMusic')
    add_child(game_scene)
    if inode != null:
        inode.queue_free()
    if music_node != null:
        music_node.queue_free()
    inode = null
    utils.timeout(self, '_start_game_4', 2.5)

var music_node
func _start_game_4():
    get_node('MenuMusic').stop()
    if MusicNode != null:
        music_node = MusicNode.instance()
        add_child(music_node)
        music_node.play()

func _on_Button_pressed():
    if starting:
        return # prevent double click
    starting = true
    next_level()

func prep_next_level():
    starting = false
    get_node('MenuMusic').set_volume_db(0)
    get_node('MenuMusic').play()
    anim.play_backwards('FadeAway')
    get_node('Menu/Victory').show()
    get_node('Menu/Logo').hide()
    get_node('Menu/NextButtonSprite').show()
    get_node('Menu/StartButtonSprite').hide()
    if current_number == 9:
        starting = true
        get_node('Menu/NextButtonSprite').hide()

