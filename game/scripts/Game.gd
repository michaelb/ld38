const VoxelTerrain = preload('res://submodules/godot-voxterrain/VoxelTerrain.tscn')
const EditCursor = preload('res://submodules/godot-voxterrain/EditCursor.tscn')

const utils = preload('./utils.gd')
var voxel_terrain = null

const ModLoader = preload('res://submodules/godot-voxmod/ModLoader.gd')
const Blockling = preload('res://scenes/Blockling.tscn')
const Spawner = preload('res://scenes/Spawner.tscn')
const GameUI = preload('res://scenes/GameUI.tscn')

var modloader = null

var current_level = 'level_1'


const BLOCK = 1
const DELETE = 2

const ARROW_BLOCK_L = 10 # <
const ARROW_BLOCK_U = 11 # ^
const ARROW_BLOCK_R = 12 # >
const ARROW_BLOCK_D = 13 # v

const LEVELS = {
    menu={
        patname='main_menu',
        count=8,
        spawner=Vector3(1, 4, 1),
    },

    level_1={
        patname='level_1',
        count=6,
        spawner=Vector3(3, 4, 1),
        items={BLOCK: 2},
    },

    level_2={
        patname='level_2',
        count=6,
        spawner=Vector3(6, 2, 1),
        items={ARROW_BLOCK_D: 1},
    },

    level_3={
        patname='level_3',
        count=6,
        spawner=Vector3(4, 4, 1),
        items={DELETE: 1},
    },

    level_4={
        patname='level_4',
        count=6,
        spawner=Vector3(7, 4, 1),
        items={
            DELETE: 1,
            ARROW_BLOCK_U: 1,
            ARROW_BLOCK_R: 1,
            BLOCK: 3,
        },
    },

    level_5={
        patname='level_5',
        count=6,
        spawner=Vector3(7, 4, 3),
        spawner_direction=3,
        items={
            DELETE: 1,
            ARROW_BLOCK_D: 2,
            BLOCK: 9,
        },
    },

    level_6={
        patname='level_6',
        count=6,
        spawner=Vector3(1, 4, 7),
        spawner_green=Vector3(1, 4, 0),
        spawner_direction=1,
        items={
            DELETE: 1,
            ARROW_BLOCK_R: 2,
            BLOCK: 2,
        },
    },

    level_7={
        patname='level_7',
        count=6,
        spawner=Vector3(7, 4, 6),
        spawner_green=Vector3(6, 4, 4),
        #spawner_direction=3,
        items={
            DELETE: 1,
            ARROW_BLOCK_D: 1,
            BLOCK: 4,
        },
    },

    level_8={
        patname='level_8',
        count=6,
        spawner=Vector3(7, 4, 7),
        spawner_green=Vector3(6, 4, 0),
        spawner_direction=3,
        items={
            DELETE: 2,
            ARROW_BLOCK_D: 2,
            ARROW_BLOCK_U: 2,
            BLOCK: 9,
        },
    },

    level_9={
        patname='level_9',
        count=6,
        spawner=Vector3(7, 4, 4),
        spawner_green=Vector3(0, 4, 0),
        spawner_direction=2,
        items={
            ARROW_BLOCK_D: 3,
            DELETE: 1,
            BLOCK: 7,
        },
    },
}

#onready var camera = get_node('PannableCamera')
var camera = null
var loader = null
onready var bg = get_node('BG')
func _ready():
    randomize()
    modloader = ModLoader.new()
    modloader.load_dir('res://voxmod')

    # Set up voxel terrain and entity manager
    voxel_terrain = VoxelTerrain.instance()
    voxel_terrain.set_mod(modloader)

    add_child(voxel_terrain)
    _load_level(current_level)

    if current_level == 'menu':
        start()
    else:
        show_ui()

var is_restarting = false
func restart():
    if is_restarting:
        return
    is_restarting = true
    if sp != null:
        sp.kill_children()
    # If already started, wait for the dying animation, otherwise just quickly
    # restart
    if started:
        utils.timeout(self, '_restart', 2)
    else:
        _restart()

func _restart():
    loader.restart()

func _load_level(level):
    # If we are loading a pattern to edit
    var level_info = LEVELS[level]
    var pattern = modloader.get('pattern', level_info.patname)
    voxel_terrain.set_pattern(pattern)
    voxel_terrain.render_all()
    #bg.look_at(camera.get_translation(), Vector3(0, 1, 0))
    #print(camera.get_rotation_deg())

var sp = null
var sp_green = null
var started = false
func start():
    started = true
    var level_info = LEVELS[current_level]
    sp = Spawner.instance()
    var spd = 2
    if level_info.has('spawner_direction'):
        spd = level_info.spawner_direction
    sp.setup(true, spd)
    sp.set_voldata(voxel_terrain.voldata)

    if level_info.has('spawner_green'):
        sp_green = Spawner.instance()
        sp_green.setup(false, spd)
        sp_green.set_voldata(voxel_terrain.voldata)
        sp_green.set_count(level_info.count / 2)
        sp.set_count(level_info.count / 2)
    else:
        sp.set_count(level_info.count)

    if gui != null:
        gui.set_spawner(sp)
        if sp_green:
            gui.set_spawner(sp_green)
    add_child(sp)
    sp.set_at_block(level_info.spawner)
    if sp_green:
        add_child(sp_green)
        sp_green.set_at_block(level_info.spawner_green)

func check_for_win(number_ascended):
    var level_info = LEVELS[current_level]
    if number_ascended >= level_info.count:
        win()

var gui = null
func show_ui():
    var level_info = LEVELS[current_level]
    if gui != null:
        gui.queue_free()
    gui = GameUI.instance()
    gui.set_game(self)
    add_child(gui)
    gui.set_terrain(voxel_terrain, camera)
    gui.set_items(level_info.items)

func win():
    gui.hide()
    gui.cursor.hide()
    gui.cursor.set_process_input(false)
    loader.prep_next_level()

#var edit_cursor = EditCursor.instance()
#edit_cursor.set_camera(camera)
#edit_cursor.add_to_terrain(voxel_terrain)
#add_child(edit_cursor)
