export(int, 0, 4) var direction = 2

onready var anim = get_node('AnimationPlayer')

signal die
signal ascend
signal rotate

const BODY_COUNT = 4
const FACE_COUNT = 6
const HEAD_COUNT = 7
const SMILE_COUNT = 4

# UGH AWFUL COIDE
const B1 = preload('res://images/blheads/body_1.png')
const B2 = preload('res://images/blheads/body_2.png')
const B3 = preload('res://images/blheads/body_3.png')
const B4 = preload('res://images/blheads/body_4.png')
var B_IMAGES = [B1, B2, B3, B4]

const F1 = preload('res://images/blheads/face_1.png')
const F2 = preload('res://images/blheads/face_2.png')
const F3 = preload('res://images/blheads/face_3.png')
const F4 = preload('res://images/blheads/face_4.png')
const F5 = preload('res://images/blheads/face_5.png')
const F6 = preload('res://images/blheads/face_6.png')
var F_IMAGES = [F1, F2, F3, F4, F5, F6]

const H1 = preload('res://images/blheads/head1.png')
const H2 = preload('res://images/blheads/head2.png')
const H3 = preload('res://images/blheads/head3.png')
const H4 = preload('res://images/blheads/head4.png')
const H5 = preload('res://images/blheads/head5.png')
const H6 = preload('res://images/blheads/head6.png')
const H7 = preload('res://images/blheads/head7.png')
var H_IMAGES = [H1, H2, H3, H4, H5, H6, H7]

const S1 = preload('res://images/blheads/smile_1.png')
const S2 = preload('res://images/blheads/smile_2.png')
const S3 = preload('res://images/blheads/smile_3.png')
const S4 = preload('res://images/blheads/smile_4.png')
var S_IMAGES = [S1, S2, S3, S4]

var speed = 1
const GRAVITY = 2

const GREEN = Color(0.5, 1, 0.5)
const BLUE = Color(0.5, 0.5, 1)

var color_type = BLUE
var initial_direction = 2

func _ready():
    set_process(true)
    anim.play('IdleAnim1')
    set_direction(initial_direction)
    _randomize_body_parts()
    _set_color(color_type)

func _randomize_body_parts():
    _randomize_sprite(get_node('Spatial/Head'), H_IMAGES)
    _randomize_sprite(get_node('Spatial/Head1'), H_IMAGES)
    _randomize_sprite(get_node('Spatial/Face'), F_IMAGES)
    _randomize_sprite(get_node('Spatial/Smile'), S_IMAGES)
    _randomize_sprite(get_node('Spatial/Body1'), B_IMAGES)
    _randomize_sprite(get_node('Spatial/Body2'), B_IMAGES)
    _randomize_sprite(get_node('Spatial/Body3'), B_IMAGES)

func _set_color(basecolor):
    get_node('Spatial/Head').set_modulate(basecolor)
    get_node('Spatial/Head1').set_modulate(basecolor)
    get_node('Spatial/Face').set_modulate(basecolor)
    get_node('Spatial/Face').set_opacity(0.3)
    get_node('Spatial/Smile').set_modulate(Color(1, 1, 1))
    #get_node('Spatial/Smile').set_modulate(Color(1, 1, 1))
    get_node('Spatial/Smile').set_modulate(basecolor)
    get_node('Spatial/Body1').set_modulate(basecolor)
    get_node('Spatial/Body2').set_modulate(basecolor)
    get_node('Spatial/Body3').set_modulate(basecolor)

func _randomize_sprite(sprite, arr):
    var img = arr[randi() % arr.size()]
    sprite.set_texture(img)

var momentum
func set_direction(nd):
    direction = nd
    if direction == 0:                  # ^
        _set_rot(180)
        momentum = Vector3(0,  0, -1)
    elif direction == 1:                # >
        _set_rot(90)
        momentum = Vector3(1,  0, 0)
    elif direction == 2:                # v
        _set_rot(0)
        momentum = Vector3(0,  0, 1)
    else:                               # <
        momentum = Vector3(-1, 0, 0)
        _set_rot(-90)

func _set_rot(y_deg):
    set_rotation_deg(Vector3(0, y_deg, 0))

var in_rotation = false
var rot_starting
var rot_target
var rot_progress = 0
const ROT_SPEED = 0.15
func _start_rotation(y_deg):
    #get_rotation_deg()
    #set_rotation_deg(Vector3(0, y_deg, 0))
    rot_starting = get_rotation_deg()
    rot_target = Vector3(0, y_deg, 0)
    in_rotation = true
    rot_progress = 0

func _handle_rot(delta):
    rot_progress += delta
    var coef = rot_progress / ROT_SPEED
    if coef >= 1:
        set_rotation_deg(rot_target)
        #translate(Vector3(0, 0, 0.1)) # Bump forward
        in_rotation = false
    else:
        var newy = (rot_target.y - rot_starting.y) * coef
        _set_rot(newy)

func flip_direction():
    if direction == 0:                  # ^
        direction = 2
        _rotate_to_direction()
    elif direction == 1:                # >
        direction = 3
        _rotate_to_direction()
    elif direction == 2:                # v
        direction = 0
        _rotate_to_direction()
    else:                               # <
        direction = 1
        _rotate_to_direction()

func _rotate_to_direction():
    if direction == 0:                  # ^
        momentum = Vector3(0,  0, -1)
        _start_rotation(180)
    elif direction == 1:                # >
        momentum = Vector3(1,  0, 0)
        _start_rotation(90)
    elif direction == 2:                # v
        momentum = Vector3(0,  0, 1)
        _start_rotation(0)
    else:                               # <
        momentum = Vector3(-1, 0, 0)
        _start_rotation(-90)

var followed_arrow = false
func follow_arrow(arrow_number):
    if arrow_number == 11:                  # ^
        direction = 2
        _rotate_to_direction()
    elif arrow_number == 12:                # >
        direction = 3
        _rotate_to_direction()
    elif arrow_number == 13:                # v
        direction = 0
        _rotate_to_direction()
    elif arrow_number == 10:                # <
        direction = 1
        _rotate_to_direction()
    else:
        printerr('invalid arrow block')


const OFFSET = Vector3(0.2, 0, 0.2)
func _get_trans():
    return get_translation() + OFFSET

func _set_trans(v):
    return set_translation(v - OFFSET)

const fatness = 0.5
func _process(delta):
    var loc = _get_trans()
    var groundloc = loc - Vector3(0, 0.5, 0) # check whats below
    var on_ground = voldata.get_block(groundloc)

    if loc.y < -1:
        die()
        return

    if in_rotation:
        _handle_rot(delta)
        return

    if not on_ground:
        translate(delta * GRAVITY * Vector3(0, -1, 0))
        return

    # Check ground before
    var ground = voldata.get_block_material(groundloc - momentum*fatness*0.5)
    if ground >= 10 and not followed_arrow:
        # Mark that we are on an arrow so we only turn once
        followed_arrow = true
        emit_signal('rotate')
        print("ROTATING!", ground)
        follow_arrow(ground)
        return

    # Mark that we are no longer on an arrow
    if ground < 10:
        followed_arrow = false

    # Check if we should ascend
    if ground == 8 and color_type == BLUE or (ground == 9 and color_type == GREEN):
        # Ascend!
        ascend()
        return

    # check whats in front
    var new_loc = momentum * delta * speed + loc
    var colliding = voldata.get_block(new_loc + momentum*fatness)
    if colliding:
        #translate(Vector3(0, 0, -0.1)) # Bump back
        flip_direction()
    else:
        _set_trans(new_loc)

var dead = false
func die():
    if dead:
        return
    dead = true
    set_process(false)
    anim.play('Die')
    emit_signal('die')

func ascend():
    dead = true
    set_process(false)
    anim.play('Ascend')
    emit_signal('ascend')

func set_at_block(v3):
    var v = Vector3(v3.x, v3.y, v3.z)
    _set_trans(v)

var voldata
func set_voldata(vd):
    self.voldata = vd

