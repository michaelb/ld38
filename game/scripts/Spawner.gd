const Blockling = preload('res://scenes/Blockling.tscn')
var count = 0

onready var anim = get_node('AnimationPlayer')
onready var anim2 = get_node('Anim2')
const GREEN = Color(0.5, 1, 0.5)
const BLUE = Color(0.5, 0.5, 1)

signal spawn
signal ascend
signal die
signal rotate


func _ready():
    anim.play('Spin')
    anim2.play_backwards('Shrink')
    set_process(true)
    #_set_color(BLUE)
    _set_color(color)
    if initial_direction != 2 and initial_direction != 0:
        set_rotation_deg(Vector3(0, 90, 0))

var initial_direction
var color
func setup(is_blue, direction):
    if is_blue:
        self.color = BLUE
    else:
        self.color = GREEN
    initial_direction = direction

func set_count(new_count):
    self.count = new_count

func kill_children():
    for child in children:
        child.die()
    set_process(false)
    children = []

func _set_color(basecolor):
    get_node('Spatial/PortalSprite').set_modulate(basecolor)

var countdown = 0
var stopped = false
const MAX = 2
func _process(delta):
    countdown += delta
    if countdown > MAX and count > 0:
        add_blockling()
        countdown = 0

    if count <= 0:
        stopped = true
        anim2.play('Shrink')
        set_process(false)


var children = []
func add_blockling():
    emit_signal('spawn')
    var blockling = Blockling.instance()
    blockling.color_type = self.color
    blockling.initial_direction = initial_direction
    get_parent().add_child(blockling)
    blockling.set_at_block(get_translation().floor() + Vector3(0, 1, 0))
    blockling.set_voldata(voldata)
    blockling.connect('ascend', self, 'on_ascend')
    blockling.connect('die', self, 'on_die')
    blockling.connect('rotate', self, 'on_rotate')
    children.append(blockling)
    count -= 1

var ascended_count = 0
func on_ascend():
    ascended_count += 1
    emit_signal('ascend')
func on_die():
    emit_signal('die')
func on_rotate():
    emit_signal('rotate')

func set_at_block(v3):
    var v = Vector3(v3.x, v3.y, v3.z)
    set_translation(v)

var voldata
func set_voldata(vd):
    self.voldata = vd

