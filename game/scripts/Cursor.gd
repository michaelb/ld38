#
# A cursor that can be used for editing! By default it is controlled by
# mouse. Click to add a block, right click to remove.

const MAX_ATTEMPTS = 100

var voxel_terrain = null
var camera = null
var location = Vector3(0, 0, 0)

func _ready():
    set_process_input(true)
    float_to_surface()

func set_camera(camera):
    self.camera = camera

const BLOCK = 1
const DELETE = 2

const ARROW_BLOCK_L = 10 # <
const ARROW_BLOCK_U = 11 # ^
const ARROW_BLOCK_R = 12 # >
const ARROW_BLOCK_D = 13 # v

var gui
func add_to_terrain(gui, voxel_terrain):
    self.gui = gui
    self.voxel_terrain = voxel_terrain
    set_cursor(0)

func _hide_all_arrows():
    for arr in get_node('Arrows').get_children():
        arr.hide()

func set_cursor(cursor):
    _hide_all_arrows()
    get_node('Deletion').hide()
    get_node('Box').hide()
    if cursor == 0:
        return

    if cursor == DELETE:
        get_node('Deletion').show()
        get_node('Box').hide()
        return

    get_node('Box').show()
    if cursor == ARROW_BLOCK_L:
        get_node('Arrows/Arrow2').show()
    elif cursor == ARROW_BLOCK_U:
        get_node('Arrows/Arrow4').show()
    elif cursor == ARROW_BLOCK_R:
        get_node('Arrows/Arrow3').show()
    elif cursor == ARROW_BLOCK_D:
        get_node('Arrows/Arrow1').show()

func float_to_surface():
    var voldata = self.voxel_terrain.voldata
    var attempts_left = MAX_ATTEMPTS
    var block = voldata.get_block(location)
    while block != 0 and attempts_left > 0:
        location.y += 1
        attempts_left -= 1
        block = voldata.get_block(location)
    _refresh_location()

func move_to_screen_position(mouse_pos):
    var voldata = self.voxel_terrain.voldata
    var ray = camera.project_ray_normal(mouse_pos)

    # Offset origin since it looks better when rounded this way
    var origin = camera.get_translation() + Vector3(0.5, 0, 0)

    var new_location = voldata.trace_ray_collision(origin, ray)
    if new_location == null:
        return false # Couldn't click anywhere
    location = new_location
    float_to_surface()
    _refresh_location()
    return true

func add_block():
    #print("Adding block at: ", location, ' with material ', material)
    #voxel_terrain.add_block(location, shape, material)
    float_to_surface()
    _refresh_location()

func remove_block():
    # Removes the block UNDER the cursor
    var remove_location = location + Vector3(0, -1, 0)
    if remove_location.y == 0:
        # Never remove at y = 0
        print("Can't remove base block")
        return
    print("Removing block at: ", remove_location)
    voxel_terrain.remove_block(remove_location)
    float_to_surface()
    _refresh_location()

func _refresh_location():
    # Moves to the location held by `location` variable
    var new_loc = self.voxel_terrain.get_translation()
    set_translation(new_loc + location)

func _input(ev):
    # Process clicks and mouse movement
    var success
    if ev.type == InputEvent.MOUSE_BUTTON:
        if ev.button_index == BUTTON_LEFT and ev.pressed:
            success = move_to_screen_position(ev.pos)
            print("MOVING!")
            if success:
                success = gui.attempt_action(location)
                if success:
                    float_to_surface()
                    _refresh_location()

    elif ev.type == InputEvent.MOUSE_MOTION:
        move_to_screen_position(ev.pos) # todo: might be nice to throttle

