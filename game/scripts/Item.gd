
var is_selected = false

var count = 0
var ever_had_count = false

const BLOCK = 1
const DELETE = 2

const ARROW_BLOCK_L = 10 # <
const ARROW_BLOCK_U = 11 # ^
const ARROW_BLOCK_R = 12 # >
const ARROW_BLOCK_D = 13 # v

export(int, 1, 13) var code = 1

func _ready():
    set_selected(false)
    refresh_block()

func decrement():
    count -= 1
    refresh_block()

func set_count(new_count):
    count = new_count
    refresh_block()

func set_selected(sel):
    self.is_selected = sel
    if is_selected:
        #get_node('IsSelectedLabel').show()
        get_node('CountIcon').set_modulate(Color(0.5, 0.5, 1.0))
        get_node('XIcon').set_modulate(Color(0.5, 0.5, 1.0))
        _get_icon().set_modulate(Color(0.5, 0.5, 1.0))
    else:
        #get_node('IsSelectedLabel').hide()
        get_node('CountIcon').set_modulate(Color(1, 1, 1.0))
        get_node('XIcon').set_modulate(Color(1, 1, 1.0))
        _get_icon().set_modulate(Color(1, 1, 1.0))

func _get_icon():
    var n = ''
    if code == BLOCK:
        n = 'Block'
    if code == DELETE:
        n = 'Deletion'
    if code == ARROW_BLOCK_L: # <
        n = 'Arrow1' # Correct
    if code == ARROW_BLOCK_U: # ^
        n = 'Arrow3'
    if code == ARROW_BLOCK_R: # >
        n = 'Arrow4'
    if code == ARROW_BLOCK_D: # v
        n = 'Arrow2'
    var i = get_node('Icons')
    return i.get_node(n)
func refresh_block():
    if count > 0:
        ever_had_count = true
    if not ever_had_count:
        hide()
    else:
        show()
    var text = '???'

    var icons = get_node('Icons').get_children()
    for icon in icons:
        icon.hide()
    _get_icon().show()

    get_node('TmpBlockLabel').set_text(text)
    #get_node('CountLabel').set_text(str(count) + 'x')
    get_node('CountIcon').set_frame(count)

    if count <= 0:
        set_opacity(0.5)
    else:
        set_opacity(1.0)

func _on_Button_pressed():
    if count > 0:
        get_parent().get_parent().select_item(self)
