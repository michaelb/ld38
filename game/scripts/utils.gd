

static func shuffle_list(list):
    # incorrect
    var new_list = []
    new_list.resize(list.size())
    var index_list = range(list.size())
    for item in list:
        var index_index = randi() % index_list.size()
        var index = index_list[index_index]
        new_list[index] = item
        index_list.remove(index_index)
    return new_list

static func shuffle_in_place(list):
    # Naive version that might not evenly random (randomly swaps), but doesn't
    # break (not sure why the other previous version is breaking)
    # Note to self: See:
    # https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle for
    # improvement
    for index in range(list.size()):
        var swap_index = randi() % list.size()
        # Swap the location of the two values
        var original = list[index]
        list[index] = list[swap_index]
        list[swap_index] = original

#
# Generate random right angle Vector2
static func random_right_angle():
    var i = randi() % 4
    if i == 0:
        return Vector2(1, 0)
    elif i == 1:
        return Vector2(0, 1)
    elif i == 2:
        return Vector2(-1, 0)
    else:
        return Vector2(0, -1)

# UNUSED below:

# Compares two dicts, ignoring values same
static func sets_equal(a, b):
    return a.has_all(b.keys()) and b.has_all(a.keys())

static func timeout(node, method_name, seconds):
    #  Simple setTimeout type function. Used as such:
    # timeout(self, "change_scenes", 5)
    var timer = Timer.new()
    node.add_child(timer)
    timer.connect("timeout", node, method_name)
    timer.set_wait_time(seconds)
    timer.set_one_shot(true)
    timer.start()

static func timeout_separate(node, object, method_name, seconds):
    #  Simple setTimeout type function. Used as such:
    # timeout(self, "change_scenes", 5)
    var timer = Timer.new()
    node.add_child(timer)
    timer.connect("timeout", object, method_name)
    timer.set_wait_time(seconds)
    timer.set_one_shot(true)
    timer.start()


